package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.ButtonItemDataModel;
import com.pmg.pixinx.Model.GalleryDataModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CaptureUploadActivity extends BaseActivity {

    public static final int CAP_UPLOAD_CODE = 100;
    public final int REQUEST_REMOTE_PICTURE = 101;
    public static final int COUNT = 10;

    String fCost = ""; /// Data send to UpLoadImages
    String linkUrl = ""; /// Data send to GetLInk
    int countPicture = 0;

    CapAdapterTop capAdapterTop;
    CapAdapterBottom capAdapterBottom;
    GalleryAdapter galleryAdapter;

    @BindView(R.id.recycler_top) RecyclerView recyclerViewTop;
    @BindView(R.id.recycler_bottom) RecyclerView recyclerViewBottom;
    @BindView(R.id.recycler_gallery) RecyclerView recyclerViewGallery;
    @BindView(R.id.lin_money) LinearLayout linMoney;
    @BindView(R.id.edt_money) EditText editMoney;
    @BindView(R.id.lin_cap_upload) LinearLayout linCapUpload;

    ArrayList<ButtonItemDataModel> datasTop = new ArrayList<>();
    ArrayList<ButtonItemDataModel> datasBottom = new ArrayList<>();
    ArrayList<GalleryDataModel> datasGallery = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capupload);

        ButterKnife.bind(this);
        loadLayout();
        initMenus();
        //////////// ItemTouchHelperCallback( Drag and Drop ) /////////////
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallbackCapUpload(galleryAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerViewGallery);

        ///////////////////// click View for hide keyboard //////
        linCapUpload.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });
    }


    @SuppressLint("NewApi")
    private void loadLayout() {
        capAdapterTop = new CapAdapterTop(this);
        recyclerViewTop.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewTop.setAdapter(capAdapterTop);

        capAdapterBottom = new CapAdapterBottom(this);
        recyclerViewBottom.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewBottom.setAdapter(capAdapterBottom);

        galleryAdapter = new GalleryAdapter(this);
        recyclerViewGallery.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewGallery.setAdapter(galleryAdapter);
    }


    private void initMenus() {

        int[] capImgTops = {R.drawable.gallery, R.drawable.local, R.drawable.remote};
        int[] capTitleTops = {R.string.imagegaller, R.string.localcamera, R.string.remotecamera};
        for (int i = 0; i < capImgTops.length; i++) {
            datasTop.add(new ButtonItemDataModel(capImgTops[i], getString(capTitleTops[i])));
        }
        capAdapterTop.setDataTop(datasTop);

        datasBottom.add(new ButtonItemDataModel(R.drawable.paid_selector, getString(R.string.paid), false));
        datasBottom.add(new ButtonItemDataModel(R.drawable.free_selector, getString(R.string.free), true));

        capAdapterBottom.setDataBottom(datasBottom);

        //// Original download call Api //////
        /*int[] galleryImage = {R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo};
        for (int i = 0; i < galleryImage.length; i++) {
            datasGallery.add(new GalleryDataModel(String.valueOf(galleryImage[i])));//"photo " + i));
        }*/
        galleryAdapter.setDataGallery(datasGallery);
    }



    /////////////// TopMenu Process ////////////
    public void onClickTopMenu(int pos) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editMoney.getWindowToken(), 0);

        switch (pos) {
            case 0 :
                ImageGallery(COUNT - countPicture);
                break;
            case 1:
                LocalCamera(COUNT - countPicture);
                break;
            case 2:
                RemoteCamera(COUNT - countPicture);
                break;
        }
    }
    /////////////// TopMenu Process End ////////////



    /////////////// BottomMenu Process ////////////
    public void onClickBottomMenu(int pos) {

        if (pos == 0){
            editMoney.requestFocus();
            linMoney.setVisibility(View.VISIBLE);


            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editMoney, InputMethodManager.SHOW_FORCED);

            /*InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }*/
        }
        else
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editMoney.getWindowToken(), 0);
            linMoney.setVisibility(View.INVISIBLE);
        }

//        for (ButtonItemDataModel one : datasBottom) {
//        }
        for (int i = 0; i < datasBottom.size(); i++) {

            ButtonItemDataModel one = datasBottom.get(i);
            one.setSelected(i == pos);
        }

        capAdapterBottom.setDataBottom(datasBottom);
       // capAdapterBottom.notifyDataSetChanged();
    }
    /////////////// BottomMenu Process End ////////////



    /*/////////////// Gallery Process ////////////
    public void onClickGallery(int pos) {
        GalleryDataModel gallery = datasGallery.get(pos);
        showToast("Gallery Photo : " + pos + " : " + gallery.getImageSrc());
        // process.
    }*/
    public void onClickClose(int pos) {

        datasGallery.remove(pos);
        galleryAdapter.notifyDataSetChanged();
        countPicture--;
        showToast(String.format(getText(R.string.max_can_select).toString(), COUNT - countPicture));
    }


    /////////////// Gallery Process End////////////
    private void ImageGallery(int number){
        if (number ==0)
        {
            showToast(getText(R.string.max_reached).toString());
            return;
        }
        ImagePicker.create(this)
                .returnMode(ReturnMode.NONE) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle(getString(R.string.imagegalleryfolders)) // folder selection title
                .toolbarImageTitle(getString(R.string.selectemptypic)) // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .includeVideo(false) // Show video on image picker
                //.single() // single mode
                .multi() // multi mode (default mode)
                .limit(number) // max images can be selected (99 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                //.origin(images) // original selected images, used in multi mode
                //.exclude(images) // exclude anything that in image.getPath()
                //.excludeFiles(files) // same as exclude but using ArrayList<File>
                //.theme(R.style.CustomImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log
                //.imageLoader(new GrayscaleImageLoder()) // custom image loader, must be serializeable
                .start(); // start image picker activity with request code
    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {

        if (requestCode == CAP_UPLOAD_CODE) {

            if (resultCode == RESULT_OK) {
                finish();
            }

            return;
        }

        /////////////  Get data from RemotePicturesActivity /////////////////
        if (requestCode == REQUEST_REMOTE_PICTURE) {

            if (resultCode == RESULT_OK) {

                ArrayList<String> temps = data.getStringArrayListExtra(RemotePicturesActivity.INTENT_DATA_PICTURES);

                for (String one : temps) {
                    GalleryDataModel temp = new GalleryDataModel(one);
                    datasGallery.add(temp);
                    countPicture = countPicture + 1;
                }

                galleryAdapter.notifyDataSetChanged();
            }

            return;
        }


        // Image Gallery
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            for (Image one : images )
            {
                datasGallery.add(new GalleryDataModel(one.getPath()));
                galleryAdapter.notifyDataSetChanged();
                countPicture++;
            }
            // or get a single image only
          /* Image image = ImagePicker.getFirstImageOrNull(data);
           pictures.add(new PictureModel(image.getPath()));
           adapter.notifyDataSetChanged();*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void LocalCamera(int number){
        if (number ==0)
        {
            showToast(getText(R.string.max_reached).toString());
            return;
        }
        ImagePicker.cameraOnly().start(this); // Could be Activity, Fragment, Support Fragment

        // You could also get the Intent
        //ImagePicker.cameraOnly().getIntent(context);
    }


    private void RemoteCamera(int number){
        if (number ==0)
        {
            showToast(getText(R.string.max_reached).toString());
            return;
        }

        /////////// Send REQUEST_REMOTE_PICTURE to RemotePicturesActivity ////////////
        Intent intent = new Intent(this, RemotePicturesActivity.class);
        intent.putExtra("COUNT", number);
        startActivityForResult(intent, REQUEST_REMOTE_PICTURE);
    }



    @OnClick(R.id.backtop)
    void onBackTop(){
        finish();
    }


    @OnClick(R.id.tv_submit)
    void onSubmit(){

       /* String order = "";
        for (GalleryDataModel one : datasGallery) {

            order += one.getImageSrc() + ", ";
            Log.d("ORDER", one.getImageSrc());
        }
        showToast(order);*/

        float minPrice = EasyPreference.with(this).getFloat("easyMinPrice", 0.0f);
        if (linMoney.isShown()) {

            if (editMoney.length() == 0) {
                editMoney.setBackgroundResource(R.drawable.edit_red_line);
                return;
            }

            fCost = editMoney.getText().toString();
            if (Float.parseFloat( fCost ) < minPrice) {

                showToast(getText(R.string.min_price).toString() + minPrice);
                editMoney.setBackgroundResource(R.drawable.edit_red_line);
                return;
            }
        }
        else {
            fCost = "";
        }


        if ( datasGallery.size() == 0){
            View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_select_pic, null, false);
            AlertDialog pinDialog = new AlertDialog.Builder(this).create();
            pinDialog.setView(pinDialogView);

            TextView txvOk = (TextView) pinDialogView.findViewById(R.id.tv_ok);
            txvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pinDialog.hide();
                }
            });
            pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pinDialog.show();
            return;
        }


        List<String> images = new ArrayList<>();

        for (GalleryDataModel one : datasGallery )
        {
            try {
                images.add( one.getImageSrc() );
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        upLoadImages(images);
    }


    ////////////// Upload Images ///////////////////

    private void upLoadImages(List<String> images) {

        showProgress();

        Intent intent = new Intent(this, GetLinkActivity.class);

        int userId = EasyPreference.with(this).getInt("easyUserId", 0);
        int waterMark = EasyPreference.with(this).getInt("easyWaterMark", 0);

        boolean enableWaterMark = EasyPreference.with(this).getBoolean("easyEnableWaterMark", false);

        ApiManager.uploadImages(enableWaterMark, userId, fCost, waterMark, images, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonObject link_info = (JsonObject) resultParam;

                        linkUrl = link_info.get("url").getAsString();

                        intent.putExtra("link_url", linkUrl);

                        startActivityForResult(intent, CAP_UPLOAD_CODE);

                }
            }
        });
    }

}


////////////// TOP Adapter //////////////////
class CapAdapterTop extends RecyclerView.Adapter<CapAdapterTop.ViewHolder> {

    CaptureUploadActivity context;
    private LayoutInflater mInflater;
    ArrayList<ButtonItemDataModel> datasourceTop = new ArrayList<>();

    public CapAdapterTop(CaptureUploadActivity context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDataTop(ArrayList<ButtonItemDataModel> list) {
        this.datasourceTop = list;
        notifyDataSetChanged();
    }

    // inflates the cell layout from xml when needed
    @Override
    public CapAdapterTop.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_button_vertical, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ButtonItemDataModel dataTop = datasourceTop.get(position);
        holder.buttonImage.setImageResource(dataTop.getImageSrc());
        holder.buttonText.setText(dataTop.getScriptText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickTopMenu(position);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceTop.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.butonimage)
        ImageView buttonImage;
        @BindView(R.id.buttontext)
        TextView buttonText;
        @BindView(R.id.buttonitem)
        LinearLayout buttonItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

///////////////BOTTOM Adapter //////////////////
class CapAdapterBottom extends RecyclerView.Adapter<CapAdapterBottom.ViewHolder> {

    CaptureUploadActivity context;
    private LayoutInflater mInflater;
    ArrayList<ButtonItemDataModel> datasourceBottom = new ArrayList<>();

    public CapAdapterBottom(CaptureUploadActivity context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDataBottom(ArrayList<ButtonItemDataModel> list) {
        this.datasourceBottom = list;
        notifyDataSetChanged();
    }

    // inflates the cell layout from xml when needed
    @Override
    public CapAdapterBottom.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_button_horizental, parent, false);
        return new CapAdapterBottom.ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(CapAdapterBottom.ViewHolder holder, int position) {

        ButtonItemDataModel dataBottom = datasourceBottom.get(position);
        holder.buttonImage.setImageResource(dataBottom.getImageSrc());
        holder.buttonText.setText(dataBottom.getScriptText());

        holder.buttonItem.setSelected(dataBottom.isSelected());
        holder.buttonImage.setSelected(dataBottom.isSelected());
        holder.buttonText.setSelected(dataBottom.isSelected());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickBottomMenu(position);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceBottom.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.butonimage)   ImageView buttonImage;
        @BindView(R.id.buttontext)   TextView buttonText;
        @BindView(R.id.buttonitem)   LinearLayout buttonItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

///////////////GALLERY Adapter //////////////////
class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> implements SimpleItemTouchHelperCallbackCapUpload.ItemTouchHelperAdapter {

    CaptureUploadActivity context;
    private LayoutInflater mInflater;
    ArrayList<GalleryDataModel> datasourceGallery = new ArrayList<>();

    public GalleryAdapter(CaptureUploadActivity context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDataGallery(ArrayList<GalleryDataModel> list) {
        this.datasourceGallery = list;
        notifyDataSetChanged();
    }

    // inflates the cell layout from xml when needed
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_gallery, parent, false);
        return new GalleryAdapter.ViewHolder(view);
    }
    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {

        GalleryDataModel dataGallery = datasourceGallery.get(position);
        //holder.galleryImage.setImageResource(Integer.parseInt(dataGallery.getImageSrc()));

        /*holder.galleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickGallery(position);
            }
        });*/
        holder.galleryClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickClose(position);
            }
        });

        /// Display Images selected in Gallery
        Glide.with(context).load(dataGallery.getImageSrc()).into(holder.galleryImage);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceGallery.size();
    }

    ///////////////  Drag and Drop ///////////
    @Override
    public void onItemDismiss(int position) {
        datasourceGallery.remove(position);
//        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(datasourceGallery, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(datasourceGallery, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.galleryimage) ImageView galleryImage;
        @BindView(R.id.galleryclose) ImageView galleryClose;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

////////////////////////////////////////// SimpleItemtouchCalss for Movenent Item /////////////////////////////////////
class SimpleItemTouchHelperCallbackCapUpload extends ItemTouchHelper.Callback {

    private final GalleryAdapter mAdapter;

    public SimpleItemTouchHelperCallbackCapUpload(
            GalleryAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        int swipeFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(),
                target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

    public interface ItemTouchHelperAdapter {

        void onItemMove(int fromPosition, int toPosition);

        void onItemDismiss(int position);
    }
}
