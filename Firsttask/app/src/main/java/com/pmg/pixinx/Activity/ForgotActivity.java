package com.pmg.pixinx.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotActivity extends BaseActivity {

    @BindView(R.id.edt_email) EditText email;

    @BindView(R.id.lin_forgot)
    LinearLayout linForgot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        ButterKnife.bind(this);

        ///////////////////// click View for hide keyboard //////
        linForgot.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });
    }


    @OnClick(R.id.tv_submit)
    void onSubmit(){

        String mail = email.getText().toString();

        if(!validation(mail))
            return;

//        Api communication.
        goApi(mail);

    }


    private void goApi(String mail){

        showProgress();

        ApiManager.forgot(mail, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:

                        if (resultParam == null){

                            showToast(getText(R.string.nointernet).toString());
                        }
                        else {
                            showToast(getText(R.string.wrong_email).toString());
                        }
                        break;

                    case SUCCESS:
                        showToast(getText(R.string.password_sent).toString());
                        finish();
                        break;
                }
            }
        });
    }



    @OnClick(R.id.tv_back)
    void onBack(){
        finish();
    }


    private  boolean validation(String mail){

        boolean flag = false;
        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag =true;
        } else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag =false;
        }
        return flag;
    }
}
