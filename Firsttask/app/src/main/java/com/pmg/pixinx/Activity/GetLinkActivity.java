package com.pmg.pixinx.Activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetLinkActivity extends BaseActivity {

    @BindView(R.id.edt_email) EditText email;
    @BindView(R.id.edt_link) TextView Link;

    @BindView(R.id.lin_getlink)
    LinearLayout linGetLink;

    ///// Intent Data Extra  //////
    Intent intent;
    String linkUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getlink);

        ButterKnife.bind(this);

        ///////// Camera Type and Cost ///////////////
        intent = getIntent();
        linkUrl = intent.getStringExtra("link_url");

        Link.setText(removeWord(linkUrl, "http://"));

        ///////////////////// click View for hide keyboard //////
        linGetLink.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });

    }


    @OnClick(R.id.booth_tv_send_link)
    void onSendLink(){
        if (!validation(email.getText().toString())){
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { email.getText().toString()});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.notice_mail));
        intent.putExtra(Intent.EXTRA_TEXT, R.string.notice_mail_body + linkUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.notice_mail_title)));

    }


    @OnClick(R.id.booth_tv_print)
    void onPrint(){
        showToast("Print");
    }


    @OnClick(R.id.tv_back_home)
    void onBack(){
        setResult(RESULT_OK);
        finish();
    }


    private  boolean validation(String mail){

        boolean flag = false;
        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag =true;
        } else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag =false;
        }

        return flag;
    }

}
