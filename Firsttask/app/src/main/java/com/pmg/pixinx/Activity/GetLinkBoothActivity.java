package com.pmg.pixinx.Activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetLinkBoothActivity extends BaseActivity {

    @BindView(R.id.edt_email) EditText email;
    @BindView(R.id.edt_link) TextView Link;

    @BindView(R.id.lin_getlink)
    LinearLayout linGetLink;

    ///// Intent Data Extra  //////
    Intent intent;
    String linkUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getlink_booth);

        ButterKnife.bind(this);

        ///////// Camera Type and Cost ///////////////
        intent = getIntent();
        linkUrl = intent.getStringExtra("link_url");
        Link.setText(removeWord(linkUrl, "http://"));

        ///////////////////// click View for hide keyboard //////
        linGetLink.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });

    }


    @OnClick(R.id.booth_tv_send_link)
    void onSendLink(){

        if (!validation(email.getText().toString())){
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { email.getText().toString()});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.notice_mail));
        intent.putExtra(Intent.EXTRA_TEXT, R.string.notice_mail_body + linkUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.notice_mail_title)));
    }


    @OnClick(R.id.booth_tv_print)
    void onPrint(){
        showToast("Print");
    }


    @OnClick(R.id.booth_tv_back)
    void onBack(){
        setResult(RESULT_OK);
        finish();
    }


    @OnClick(R.id.getlink_tv_quit)
    void onQuit(){

        int myPINCode = EasyPreference.with(this)
                .getInt("easyPINCode", 0);

        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_pin_code, null, false);
        AlertDialog pinDialog = new AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);

        TextView txvPinCancel = (TextView) pinDialogView.findViewById(R.id.tv_pin_cancel);
        TextView txvPinOk = (TextView) pinDialogView.findViewById(R.id.tv_pin_ok);

        TextView txvPin = (TextView) pinDialogView.findViewById(R.id.edit_pin);

        txvPinCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });


        txvPinOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txvPin.getText().toString().isEmpty())
                    return;

                if (myPINCode == Integer.parseInt(txvPin.getText().toString()))
                {
                    pinDialog.hide();

                    Intent intent = new Intent(GetLinkBoothActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); /// Once remove the Activities between this and HomeActivity opened before already
                    startActivity(intent);

                    finish();
                }
                else
                {
                    showToast(getText(R.string.confirmpincode).toString());
                }
            }
        });

        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();

    }



    private  boolean validation(String mail){

        boolean flag = false;

        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag =true;
        } else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag =false;
        }

        return flag;
    }

}
