package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Base.HomeRecyclerViewAdapter;
import com.pmg.pixinx.Model.ButtonItemDataModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {

    private final int REQUEST_CODE_HOME = 100;

    HomeRecyclerViewAdapter adapter;
    ArrayList<ButtonItemDataModel> datas = new ArrayList<>();

    @BindView(R.id.recycler) RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        loadLayout();

        initMenus();

        //////// printer URL and HOW_TO_USE URL getting //////////////
        goGetting();
    }


    @SuppressLint("NewApi")
    private void loadLayout() {

        adapter = new HomeRecyclerViewAdapter(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
    }


    private void initMenus() {

        int[] homeImgIds = {R.drawable.upload, R.drawable.camera, R.drawable.history, R.drawable.links, R.drawable.printer, R.drawable.setting};
        int[] homeMenuTitles = {R.string.captureandupload, R.string.photoboth, R.string.saleshistory, R.string.mylinks, R.string.purchaseprinter, R.string.settings};

        for (int i = 0; i < homeImgIds.length; i++) {
            datas.add(new ButtonItemDataModel(homeImgIds[i], getString(homeMenuTitles[i])));
        }

        adapter.setData(datas);
    }

    private void goGetting(){

        ApiManager.getSettings(new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonObject json = (JsonObject) resultParam;

                        EasyPreference.with(HomeActivity.this).addString("easyHowToUse", json.get("help_link").getAsString()).save();
                        EasyPreference.with(HomeActivity.this).addString("easyPrinterUrl", json.get("printer_link").getAsString()).save();
                        EasyPreference.with(HomeActivity.this).addFloat("easyMinPrice", json.get("min_price").getAsFloat()).save();
                }
            }
        });
    }

    /*************** Home Menus *****************/
    public void onClickMenu(int pos) {

         switch (pos) {

            case 0 :
                goUploadPic();
                break;
              case 1:
                goPhotobooth();
                break;
            case 2:
                goSalesHis();
                break;
            case 3:
                goMylinks();
                break;
            case 4:
                goPurPrinter();
                break;
            case 5:
                goSettings();
                break;
        }
    }


    private void goUploadPic(){
         Intent intent = new Intent(this, CaptureUploadActivity.class);
         startActivity(intent);
    }


    private void goPhotobooth(){
        Intent intent = new Intent(this, PhotoBoothActivity.class);
        startActivity(intent);
    }


    private void goSalesHis(){
        Intent intent = new Intent(this, SalesHistoryActivity.class);
        startActivity(intent);
    }


    private void goMylinks(){
        Intent intent = new Intent(this, MylinksActivity.class);
        startActivity(intent);
    }


    private void goPurPrinter(){
        Intent intent = new Intent(this, PurPrinterActivity.class);
        startActivity(intent);
    }


    private void goSettings(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_HOME);
    }


    @OnClick(R.id.tv_usage)
    void onUsage(){
        Intent intent = new Intent(this, HowToUseActivity.class);
        startActivity(intent);
    }

    /****************** LogOut Process Part ******************/
    @OnClick(R.id.tv_logout)
    void txtLogout(){
        onLogout();
    }
    @OnClick(R.id.img_logout)
    void imgLogout(){
        onLogout();
    }
    void onLogout(){
        Intent intent = new Intent(this, SigninActivity.class);

        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_logout_confirm, null, false);
        androidx.appcompat.app.AlertDialog pinDialog = new AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);

        TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
        TextView txvLogout = (TextView) pinDialogView.findViewById(R.id.tv_logout);

        txvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });

        txvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
                startActivity(intent);

                ////////////  Sign save data clear /////////////
                EasyPreference.with(HomeActivity.this)
                        .clearAll()
                        .save();

                finish();
            }
        });

        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_HOME){

            if (resultCode == RESULT_OK){ //// SettingsActivity's Logout event.

                finish();
            }
        }
    }
}
