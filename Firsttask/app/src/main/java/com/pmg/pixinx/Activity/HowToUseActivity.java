package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HowToUseActivity extends BaseActivity {

    @BindView(R.id.web) WebView webView;
    @BindView(R.id.progressBar) ProgressBar pbar;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_howtouse);

        ButterKnife.bind(this);

        String Url = EasyPreference.with(this).getString("easyHowToUse", "");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);     // JavaScript Event Enable
        webView.setWebViewClient(new WebViewClient());
        /*webView.loadUrl("http://www.pixinx.com/");*/
        webView.loadUrl(Url);

        /*String result;
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.terms);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            // e.printStackTrace();
            result = "Error: can't show file.";
        }

        tvterms.setText(result);*/
    }



    @OnClick(R.id.back)
    void onBack(){
        finish();
    }


    /******************* WebView Client Medel **********************/
    public class WebViewClient extends android.webkit.WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {

            // TODO Auto-generated method stub

            super.onPageFinished(view, url);
            pbar.setVisibility(View.GONE);

        }

    }
}
