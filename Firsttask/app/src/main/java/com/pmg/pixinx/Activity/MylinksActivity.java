package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.CustomView.URLSpanNoUnderline;
import com.pmg.pixinx.Model.SalesModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MylinksActivity extends BaseActivity {

    MylinksAdapter mylinksAdapter;
    ArrayList<SalesModel> datasSales = new ArrayList<>();

    public static final int REQUEST_CODE_MYLINKS = 100;

    @BindView(R.id.recyclerview_links) RecyclerView recyclerViewLnks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylinks);

        ButterKnife.bind(this);

        loadLayout();

        initMenus();
    }


    @SuppressLint({"NewApi", "WrongConstant"})
    private void loadLayout() {

        mylinksAdapter = new MylinksAdapter(this);
        recyclerViewLnks.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewLnks.setAdapter(mylinksAdapter);
    }


    private void initMenus() {

        ///////////////////// UI VERSION /////////////////
        /*//// Original download call Api //////
        int[] myLinksImage = {R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo};
        String[] myLinksURL = {getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext)};
        Date[] mylilnksdate = {new Date(), new Date(), new Date(), new Date(), new Date(), new Date()};
        float[] mylnikscost = {5.6f, 5.0f, 0.0f, 6.5f, 0f, 3.6f};

        for (int i = 0; i < myLinksImage.length; i++) {
            datasSales.add(new SalesModel(String.valueOf(myLinksImage[i]), myLinksURL[i], mylilnksdate[i], mylnikscost[i]));
        }

        mylinksAdapter.setDataHisLinks(datasSales);*/

        showProgress();

        int userID = EasyPreference.with(this).getInt("easyUserId", 0);

        ApiManager.myLinks(userID, new ICallback() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonArray imagesJsonArray = (JsonArray) resultParam;

                        if (imagesJsonArray.size() != 0) {

                            for (int i=0 ; i < imagesJsonArray.size(); i++){

                                JsonObject json = imagesJsonArray.get(i).getAsJsonObject();

                                datasSales.add(new SalesModel(json.get("id").getAsInt(), json.get("url").getAsString(), json.get("price").getAsFloat(), json.get("images").getAsString(), json.get("date").getAsString()));

                            }

                            ///////////////   Sort part ///////////////////
                            Object[] sales = datasSales.toArray();   ////////// ArrayList<SalesModel> to object type
                            Arrays.sort(sales);   ///////// Sort

                            for ( int i = 0 ; i < sales.length; i++ )
                            {
                                datasSales.set(i, (SalesModel) sales[i]);   //////// Again, object type to ArrayList<SalesModel>
                            }

                            ///////////// !!!!!!!  This code one line is enough for sort. ////////////////////
                            /*datasSales.sort(SalesModel::compareTo);*/

                            ///////////////   Sort part End ///////////////////

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mylinksAdapter.setDataMyLinks(datasSales);
                                }
                            });

                        }
                        else
                        {
                            showToast(getText(R.string.no_register_product).toString());
                        }
                        break;
                }
            }
        });
    }


    /////////////// MyLinks Process ////////////
    public void onClickMyLinksItem(String linkUrl) {
        Intent intent = new Intent(this, GetLinkActivity.class);
        intent.putExtra("link_url", linkUrl);
        startActivityForResult(intent, REQUEST_CODE_MYLINKS);

    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_MYLINKS) {

            if (resultCode == RESULT_OK) {
                finish();
            }

            return;
        }
    }


    @OnClick(R.id.imv_back)
    void onMyLinksBack(){
        finish();
    }

}




///////////******************* MylinksAdapter Adapter ***************** //////////////////
class MylinksAdapter extends RecyclerView.Adapter<MylinksAdapter.ViewHolder> {

    MylinksActivity context;
    private LayoutInflater mInflater;
    ArrayList<SalesModel> datasourceMyLinks = new ArrayList<>();


    public MylinksAdapter(MylinksActivity context) {

        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }


    public void setDataMyLinks(ArrayList<SalesModel> list) {

        this.datasourceMyLinks = list;
        notifyDataSetChanged();
    }


    // inflates the cell layout from xml when needed
    @Override
    public MylinksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_links, parent, false);
        return new MylinksAdapter.ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MylinksAdapter.ViewHolder holder, int position) {

        SalesModel dataMyLink = datasourceMyLinks.get(position);

//        holder.mylilnksImg.setImageResource(Integer.parseInt(dataMyLink.getImgSrc()));
        String duplicatedUrl = dataMyLink.getImgSrc();
        String[] separatedUrl = duplicatedUrl.split(",");
        Glide.with(context).load(separatedUrl[0]).placeholder(R.drawable.thumb).into(holder.mylilnksImg);

        ///// Remove underline //////
        /*holder.mylinksUrl.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='"+dataMyLink.getLinkAdress()+"'>"+dataMyLink.getLinkAdress()+"</a>";
        holder.mylinksUrl.setText(Html.fromHtml(text));
        stripUnderlines(holder.mylinksUrl);*/
        holder.mylinksUrl.setText(dataMyLink.getLinkAdress());

        ///// Date and time separated //////////////////////
        String dateAndtime = String.valueOf(dataMyLink.getSalesDate());
        /*String[] separated = dateAndtime.split(" ");
        holder.mylinksDate.setText(separated[0]);*/
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
        Date strDate = null;
        try {
            strDate = sdf.parse(dateAndtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mylinksDate.setText( sdf1.format(strDate) );


        if( dataMyLink.getCost() == 0)
            holder.mylinksCost.setText(context.getString(R.string.free));
        else
            holder.mylinksCost.setText(String.valueOf(dataMyLink.getCost()));


        if( dataMyLink.getCost() == 0){
            holder.mylinksCostBack.setText(context.getString(R.string.free));
            holder.mylinksCostBack.setSelected(false);
            holder.mylinksCost.setVisibility(View.INVISIBLE);
        }
        else {
            holder.mylinksCostBack.setText(context.getString(R.string.paid));
            holder.mylinksCostBack.setSelected(true);
            holder.mylinksCost.setText("$" + String.valueOf(dataMyLink.getCost()));
            holder.mylinksCost.setVisibility(View.VISIBLE);
        }

        holder.dataMyLinkItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickMyLinksItem(dataMyLink.getLinkAdress());
            }
        });
    }


    ////************ Remove Underline **************//////

    private void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceMyLinks.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.img_mylinksimage)ImageView mylilnksImg;
        @BindView(R.id.tv_mylinksurl) TextView mylinksUrl;
        @BindView(R.id.tv_mylinksdate) TextView mylinksDate;
        @BindView(R.id.tv_mylinks_cost) TextView mylinksCost;
        @BindView(R.id.tv_mylinkscost_back) TextView mylinksCostBack;
        @BindView(R.id.lin_mylinksitem) LinearLayout dataMyLinkItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}