package com.pmg.pixinx.Activity;


import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.CustomView.URLSpanNoUnderline;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentActivity extends BaseActivity {

    @BindView(R.id.edt_email) EditText email;
    @BindView(R.id.tv_paypalurl) TextView paypalUrl;

    @BindView(R.id.lin_payment)
    LinearLayout linPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        ButterKnife.bind(this);

/////////////////////  Link Underline Remove  /////////////////
        ///// Remove underline //////
        paypalUrl.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='"+paypalUrl.getText()+"'>"+paypalUrl.getText()+"</a>";
        paypalUrl.setText(Html.fromHtml(text));
        stripUnderlines(paypalUrl);
////////////////////////////////////////

        ///////////// show keyboard when begin Activity start ////////
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        email.requestFocus();
        imm.showSoftInput(email, 0);
        /*imm.showSoftInput(email, InputMethodManager.SHOW_FORCED);*/

        ///////////////////// click View for hide keyboard //////
        linPayment.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });
    }

    private void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    @OnClick(R.id.paypal_tv_submit)
    void onSendLink(){

        if (!validation(email.getText().toString())){
            return;
        }

        goApi(email.getText().toString());
    }


    private void goApi(String paypalmail){

        int myID = EasyPreference.with(this)
                .getInt("easyUserId", 0);

        showProgress();

        ApiManager.payment(myID, paypalmail, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:

                        if (resultParam == null){

                            showToast(getText(R.string.nointernet).toString());
                        }
                        else {
                            showToast(getText(R.string.email_not_exist).toString());
                        }
                        break;

                    case SUCCESS:
                        showToast(getText(R.string.paypal_sent).toString());
                        finish();
                        break;
                }
            }
        });
    }
    @OnClick(R.id.back)
    void onBack(){
        finish();
    }


    private  boolean validation(String mail){

        boolean flag = false;

        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag =true;
        }
        else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag =false;
        }

        return flag;
    }



}