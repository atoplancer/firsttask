package com.pmg.pixinx.Activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Base.EasyPrefClass;
import com.pmg.pixinx.R;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoBoothActivity extends BaseActivity {

    private final int REQUEST_CODE_BOOTH = 100;

    String bCamera = "local";
    String fCost = "";

    @BindView(R.id.booth_lin_local) LinearLayout linLocal;
    @BindView(R.id.booth_lin_remote) LinearLayout linRemote;
    @BindView(R.id.booth_lin_paid) LinearLayout linPaid;
    @BindView(R.id.booth_lin_free) LinearLayout linFree;
    @BindView(R.id.lin_money) LinearLayout linMoney;
    @BindView(R.id.edt_money) EditText editMoney;
    @BindView(R.id.lin_booth) LinearLayout linBooth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photobooth);

        ButterKnife.bind(this);
        init();

        ///////////////////// click View for hide keyboard //////
        linBooth.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });
    }


    private void init(){
        linLocal.setSelected(true);
        linFree.setSelected(true);
    }


    @OnClick(R.id.back)
    void onBack(){
        finish();
    }



    @OnClick(R.id.booth_lin_local)
    void onLocal(){
        linLocal.setSelected(true);
        linRemote.setSelected(false);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editMoney.getWindowToken(), 0);

    }


    @OnClick(R.id.booth_lin_remote)
    void onRemote(){
        linLocal.setSelected(false);
        linRemote.setSelected(true);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editMoney.getWindowToken(), 0);

    }


    @OnClick(R.id.booth_lin_paid)
    void onPaid(){
        linPaid.setSelected(true);
        linFree.setSelected(false);
        linMoney.setVisibility(View.VISIBLE);

        /*InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }*/


        editMoney.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editMoney, InputMethodManager.SHOW_FORCED);
    }


    @OnClick(R.id.booth_lin_free)
    void onFree(){
        linPaid.setSelected(false);
        linFree.setSelected(true);
        linMoney.setVisibility(View.INVISIBLE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editMoney.getWindowToken(), 0);

    }


    @OnClick(R.id.booth_tv_start)
    void onstart(){

        if (linRemote.isSelected()){
            bCamera = "remote";
        }
        else
        {
            bCamera = "local";
        }

        float minPrice = EasyPreference.with(this).getFloat("easyMinPrice", 0.0f);
        if (linPaid.isSelected()){

            if (editMoney.length() == 0) {
                editMoney.setBackgroundResource(R.drawable.edit_red_line);
                return;
            }

            fCost = editMoney.getText().toString();
            if (Float.parseFloat( fCost ) < minPrice) {

                showToast(getText(R.string.min_price).toString() + minPrice);
                editMoney.setBackgroundResource(R.drawable.edit_red_line);
                return;
            }
        }
        else {
            fCost = "";
        }

        Intent intent = new Intent(PhotoBoothActivity.this, UploadPictureActivity.class);
        intent.putExtra("camera", bCamera);
        intent.putExtra("cost", fCost);

        int myPINCode = EasyPreference.with(PhotoBoothActivity.this)
                .getInt("easyPINCode", 0);

        if (myPINCode == 0) {
            View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_pin_code_confirm, null, false);
            AlertDialog pinDialog = new AlertDialog.Builder(this).create();
            pinDialog.setView(pinDialogView);

            TextView txvPinCancel = (TextView) pinDialogView.findViewById(R.id.tv_pin_cancel);
            TextView txvPinSet = (TextView) pinDialogView.findViewById(R.id.tv_pin_set);

            TextView txvPin = (TextView) pinDialogView.findViewById(R.id.edit_pin);
            TextView txvPinConfirm = (TextView) pinDialogView.findViewById(R.id.edit_pin_confirm);

            txvPinCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pinDialog.hide();
                }
            });
            txvPinSet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (txvPin.getText().toString().isEmpty() || txvPinConfirm.getText().toString().isEmpty()){

                        showToast(getText(R.string.wrong_pin).toString());
                        return;
                    }
                    if (txvPin.getText().toString().equals(txvPinConfirm.getText().toString()))
                    {
                        pinDialog.hide();

                        EasyPreference.with(PhotoBoothActivity.this)
                            .addInt("easyPINCode", Integer.parseInt(txvPin.getText().toString()))
                            .save();

                        EasyPrefClass one = EasyPreference.with(PhotoBoothActivity.this).getObject("easySettingStates", EasyPrefClass.class);
                        one.selectedStates[6] = true;

                        EasyPreference.with(PhotoBoothActivity.this).addObject("easySettingStates", one).save();

                        startActivityForResult(intent, REQUEST_CODE_BOOTH);
                    }
                    else
                    {
                        showToast(getText(R.string.confirmpincode).toString());
                    }
                }
            });
            pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pinDialog.show();
            return;
         }

        startActivityForResult(intent, REQUEST_CODE_BOOTH);

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_BOOTH) {

            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

}