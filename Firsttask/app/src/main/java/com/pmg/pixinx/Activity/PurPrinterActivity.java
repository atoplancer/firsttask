package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurPrinterActivity extends BaseActivity {

    @BindView(R.id.web) WebView webView;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purprinter);

        ButterKnife.bind(this);

        String Url = EasyPreference.with(this).getString("easyPrinterUrl", "");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
//        webView.setWebViewClient(new WebViewClient());
        /*webView.loadUrl("https://www.amazon.com/");*/
        webView.loadUrl(Url);

        /*String result;
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.terms);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            // e.printStackTrace();
            result = "Error: can't show file.";
        }

        tvterms.setText(result);*/
    }

    @OnClick(R.id.back)
    void onBack(){
        finish();
    }
}