package com.pmg.pixinx.Activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RemotePicturesActivity extends BaseActivity {

    public static final String INTENT_DATA_PICTURES = "INTENT_DATA_PICTURES";

    Intent intent;
    int maxCount;
    int currentCount = 0;

    RemoteAdapter remoteAdapter;
    ArrayList<String> remotePictures = new ArrayList<>();
    ArrayList<String> newRemotePictures = new ArrayList<>();

    ArrayList<String> arrayRemotePictures = new ArrayList<>();
    ArrayList<Boolean> arrayChecked = new ArrayList<>();

    @BindView(R.id.recycler_remote_picture) RecyclerView recyclerRemotePicture;
    @BindView(R.id.tv_title) TextView title;
    @BindView(R.id.tv_done) TextView done;
    @BindView(R.id.tv_current_number) TextView currentNumber;
    @BindView(R.id.tv_total_number) TextView totalNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remotepictures);

        ButterKnife.bind(this);

        loadLayout();

        loadRemotePictures();

        intent = getIntent();
        maxCount = intent.getIntExtra("COUNT", 10);
    }



    @SuppressLint("NewApi")
    private void loadLayout() {

        remoteAdapter = new RemoteAdapter(this);
        recyclerRemotePicture.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerRemotePicture.setAdapter(remoteAdapter);
    }


    private void loadRemotePictures() {

        showProgress();

        String userName = EasyPreference.with(this).getString("easyUserName", "");

        ApiManager.remotePictures(userName, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonArray imagesJsonArray = (JsonArray) resultParam;

                        if (imagesJsonArray.size() != 0) {

                            for (int i=0 ; i < imagesJsonArray.size(); i++){

                                remotePictures.add(imagesJsonArray.get(i).getAsString());
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    remoteAdapter.setRemotePictures(remotePictures);
                                }
                            });

                        }
                        else
                        {
                            showToast(getText(R.string.no_remote_images).toString());
                        }
                        break;
                }
            }
        });
    }


    @OnClick(R.id.tv_done)
    void onDone(){

        for ( int i = 0; i < arrayChecked.size(); i++ )
        {
            /*if (!arrayChecked.get(i).booleanValue()) {

                remotePictures.remove(i);

            }*/
            if (arrayChecked.get(i).booleanValue()) {

                newRemotePictures.add(remotePictures.get(i));

            }
        }

        //////////////////// Data Extra to CaptureUploadActivity /////////////////
        Intent intent = new Intent();
        intent.putStringArrayListExtra(INTENT_DATA_PICTURES, newRemotePictures);
        setResult(RESULT_OK, intent);
        finish();

    }


    @OnClick(R.id.back)
    void onBack(){
        finish();
    }

}

////////////************ RemotePicture Adapter ****************//////////////////
class RemoteAdapter extends RecyclerView.Adapter<RemoteAdapter.ViewHolder> {

    RemotePicturesActivity context;
    /*ArrayList<String> arrayRemotePictures = new ArrayList<>();
    ArrayList<Boolean> arrayChecked = new ArrayList<>();*/


    private LayoutInflater mInflater;

    public RemoteAdapter(RemotePicturesActivity context) {

        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }


    public void setRemotePictures(ArrayList<String> list) {

        context.arrayRemotePictures = list;
        context.arrayChecked.clear();

        for (int i = 0; i < list.size(); i++){

            context.arrayChecked.add(i, false);
        }

        notifyDataSetChanged();
    }


    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_remote_picture, parent, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String remotePicture = context.arrayRemotePictures.get(position);

        ///holder.imgRemotePicture.setImageURI(Uri.parse(remotePicture));
        Glide.with(context).load(remotePicture).placeholder(R.drawable.thumb).into(holder.imgRemotePicture);

        holder.frmRemotePicture.setSelected(context.arrayChecked.get(position));

        holder.frmRemotePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (context.arrayChecked.get(position).booleanValue()) {   /// unselect

                    context.arrayChecked.set(position, false);
                    context.currentCount = context.currentCount - 1;

                    if( context.maxCount - context.currentCount == context.maxCount) ///// empty state
                    {
                        context.title.setVisibility(View.VISIBLE);
                        context.done.setVisibility(View.INVISIBLE);
                        context.currentNumber.setText("");
                        context.totalNumber.setText("");
                    }
                    else {
                        context.currentNumber.setText("" + ( context.currentCount ) );
                        context.totalNumber.setText("/" + context.maxCount + " selected" );
                    }
                }
                else {                                                 ///////// select
                    if (context.maxCount - context.currentCount == 0)  ///////// full state
                    {
                        context.showToast(context.getText(R.string.max_limited).toString());
                        return;
                    }

                    context.arrayChecked.set(position, true);
                    context.currentCount = context.currentCount + 1;

                    context.title.setVisibility(View.INVISIBLE);
                    context.done.setVisibility(View.VISIBLE);
                    context.currentNumber.setText("" + ( context.currentCount ) );
                    context.totalNumber.setText("/" + context.maxCount + " selected" );
                }
                notifyItemChanged(position);
            }
        });

    }




    // total number of cells
    @Override
    public int getItemCount() {
        return context.arrayRemotePictures.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.frm_remote_picture) FrameLayout frmRemotePicture;
        @BindView(R.id.img_remote_picture) ImageView imgRemotePicture;
        @BindView(R.id.img_check) ImageView imgCheck;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}