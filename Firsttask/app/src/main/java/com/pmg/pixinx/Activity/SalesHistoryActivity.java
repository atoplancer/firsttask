package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.CustomView.URLSpanNoUnderline;
import com.pmg.pixinx.Model.SalesModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SalesHistoryActivity extends BaseActivity {

    HislinksAdapter HislinksAdapter;
    ArrayList<SalesModel> datasHiss = new ArrayList<>();

    public static final int REQUEST_CODE_SALES = 100;

    float earnedCost = 0f;
    float earnedMonthCost = 0f;
    float balance = 0f;

    @BindView(R.id.recyclerview_links) RecyclerView recyclerViewLnks;
    @BindView(R.id.tv_total_earned) TextView totalEarned;
    @BindView(R.id.tv_month_earned) TextView monthEarned;
    @BindView(R.id.tv_balance_earned) TextView balanceEarned;
    @BindView(R.id.lin_total) LinearLayout linTotal;
    @BindView(R.id.lin_month) LinearLayout linMonth;
    @BindView(R.id.lin_balance) LinearLayout linBalance;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saleshis);

        ButterKnife.bind(this);
        loadLayout();
        initMenus();
    }


    @SuppressLint({"NewApi", "WrongConstant"})
    private void loadLayout() {

        HislinksAdapter = new HislinksAdapter(SalesHistoryActivity.this);
        recyclerViewLnks.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewLnks.setAdapter(HislinksAdapter);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initMenus() {

        /*//// Original download call Api //////
        int[] hisLinksImage = {R.drawable.photo, R.drawable.logo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo};

        String[] hisLinksURL = {getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext), getString(R.string.yourlinkdefaulttext)};

        *//*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());*//*
        Date[] hislilnksdate = {new Date(), new Date(), new Date(), new Date(), new Date(), new Date()};

        float[] hislinkscost = {5.6f, 5.0f, 0.0f, 6.5f, 0f, 3.6f};


        for (int i = 0; i < hisLinksImage.length; i++) {
            datasHiss.add(new SalesModel(i, hisLinksURL[i], hislinkscost[i], String.valueOf(hisLinksImage[i]), String.valueOf(hislilnksdate[i])));
        }

        HislinksAdapter.setDataHisLinks(datasHiss);

        // Earned Product Cost calculate. //
        onCalculatorEarned(datasHiss);*/

        showProgress();

        int userID = EasyPreference.with(this).getInt("easyUserId", 0);

        ApiManager.myHis(userID, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonObject jsonResult = (JsonObject) resultParam;

                        balance = jsonResult.get("balance").getAsFloat();
                        JsonArray sales = jsonResult.get("sales").getAsJsonArray();

                        SalesModel[] salesModels = new SalesModel[sales.size()];  //////// object initialize


                        if (sales.size() != 0) {

                            for (int i=0 ; i < sales.size(); i++){

                                JsonObject json = sales.get(i).getAsJsonObject();

                                datasHiss.add(new SalesModel(json.get("link_id").getAsInt(), json.get("buyer").getAsString(), json.get("price").getAsFloat(), json.get("commission").getAsFloat(), json.get("url").getAsString(), json.get("images").getAsString(), json.get("date").getAsString()));

                                salesModels[i] = datasHiss.get(i);    ////////// ArrayList<SalesModel> to object type
                            }

                            Arrays.sort(salesModels);   ///////// Sort

                            for ( int i = 0 ; i < salesModels.length; i++ )
                            {
                                datasHiss.set(i, salesModels[i]);   //////// Again, object type to ArrayList<SalesModel>
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    HislinksAdapter.setDataHisLinks(datasHiss);
                                    onCalculatorEarned(datasHiss);
                                }
                            });

                        }
                        else
                        {
                            showToast(getText(R.string.no_sales_history).toString());
                        }
                        break;
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onCalculatorEarned(ArrayList<SalesModel> dataHiss){

        balanceEarned.setText(("$" + balance));
       for (int i = 0; i < dataHiss.size(); i++){

            earnedCost += ( dataHiss.get(i).getCost() - dataHiss.get(i).getCommission() );

            // if getSalesDate in this month
            Calendar cal = Calendar.getInstance();
            int thismonth = cal.get(Calendar.MONTH);
            int thisyear = cal.get(Calendar.YEAR);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date strDate = null;
            String date = dataHiss.get(i).getSalesDate();

            try {
               strDate = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            cal.setTime(strDate);

            if (thisyear == cal.get(Calendar.YEAR) && thismonth == cal.get(Calendar.MONTH)) {
                earnedMonthCost +=  ( dataHiss.get(i).getCost() - dataHiss.get(i).getCommission() );
            }
        }

        totalEarned.setText("$" + earnedCost);
        monthEarned.setText("$" + earnedMonthCost);

        linTotal.setVisibility(View.VISIBLE);
        linMonth.setVisibility(View.VISIBLE);
        linBalance.setVisibility(View.VISIBLE);
    }



    @OnClick(R.id.hislinks_back)
    void onHisLinksBack(){
        finish();
    }

    /////////////// HisLinks Process ////////////
    public void onClickHisLinksItem(String linkAdress) {

        Intent intent = new Intent(this, GetLinkActivity.class);
        intent.putExtra("link_url", linkAdress);
        startActivityForResult(intent, REQUEST_CODE_SALES);
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_SALES) {

            if (resultCode == RESULT_OK) {
                finish();
            }

            return;
        }
    }
}


/////////////************* HislinksAdapter Adapter *****************//////////////////
class HislinksAdapter extends RecyclerView.Adapter<HislinksAdapter.ViewHolder> {

    SalesHistoryActivity context;
    private LayoutInflater mInflater;
    ArrayList<SalesModel> datasourceHisLinks = new ArrayList<>();


    public HislinksAdapter(SalesHistoryActivity context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }


    public void setDataHisLinks(ArrayList<SalesModel> list) {
        this.datasourceHisLinks = list;
        notifyDataSetChanged();
    }


    // inflates the cell layout from xml when needed
    @Override
    public HislinksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_links, parent, false);
        return new HislinksAdapter.ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(HislinksAdapter.ViewHolder holder, int position) {

        SalesModel dataHisLink = datasourceHisLinks.get(position);

        //// Image Load ////////
        String duplicatedUrl = dataHisLink.getImgSrc();
        String[] separatedUrl = duplicatedUrl.split(",");
        Glide.with(context).load(separatedUrl[0]).placeholder(R.drawable.thumb).into(holder.hislinksImg);

        ///// Remove underline //////
        /*holder.hisLinksURL.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='"+dataHisLink.getLinkAdress()+"'>"+dataHisLink.getLinkAdress()+"</a>";
        holder.hisLinksURL.setText(Html.fromHtml(text));
        holder.hisLinksURL.setLinkTextColor(context.getResources().getColor(R.color.colorGreyDark));
        stripUnderlines(holder.hisLinksURL);*/
        holder.hisLinksURL.setText(dataHisLink.getLinkAdress());
        holder.hisLinksURL.setTextColor(context.getResources().getColor(R.color.colorGreyDark));

        ///// Date and time separated //////////////////////
        String dateAndtime = dataHisLink.getSalesDate();
        /*String[] separated = dateAndtime.split(" ");
        holder.hislinksDate.setText(separated[0]);*/
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
        Date strDate = null;
        try {
            strDate = sdf.parse(dateAndtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.hislinksDate.setText( sdf1.format(strDate) );



        //////////  Cost /////////////////////
        holder.hislinks_Cost.setTextColor(context.getResources().getColor(R.color.colorGreyDark));

        if( dataHisLink.getCost() == 0){

            holder.hislinksCostBack.setText(context.getString(R.string.free));
            holder.hislinksCostBack.setSelected(false);
            holder.hislinks_Cost.setVisibility(View.INVISIBLE);
        }
        else {

            holder.hislinksCostBack.setText(context.getString(R.string.paid));
            holder.hislinksCostBack.setSelected(true);
            holder.hislinks_Cost.setText("$" + String.valueOf(dataHisLink.getCost()));
            holder.hislinks_Cost.setVisibility(View.VISIBLE);
        }

        holder.dataHisLinkItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickHisLinksItem(dataHisLink.getLinkAdress());
            }
        });
//        context.onCalculatorEarned(dataHisLink.getCost(), dataHisLink.getSalesDate());
    }


    ////************ Remove Underline **************//////

    private void stripUnderlines(TextView textView) {
        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceHisLinks.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.img_mylinksimage)ImageView hislinksImg;
        @BindView(R.id.tv_mylinksurl) TextView hisLinksURL;
        @BindView(R.id.tv_mylinksdate) TextView hislinksDate;
        @BindView(R.id.tv_mylinks_cost) TextView hislinks_Cost;
        @BindView(R.id.tv_mylinkscost_back) TextView hislinksCostBack;
        @BindView(R.id.lin_mylinksitem) LinearLayout dataHisLinkItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}