package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Base.EasyPrefClass;
import com.pmg.pixinx.Model.SettingsModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {

    SettingsAdapter settingsAdapter;
    ArrayList<SettingsModel> datasSettings = new ArrayList<>();

    @BindView(R.id.recyclerview_settings) RecyclerView recyclerViewsettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        loadLayout();
        initMenus();


        /*ToggleButton toggleBtn;

        // Toggle switch
        toggleBtn . toggle();
        // Switch no animation
        toggleBtn . toggle( false );
        // Switch toggle event
        toggleBtn . setOnToggleChanged( new  ToggleButton.OnToggleChanged(){
            @Override
            public  void  onToggle ( boolean  on ) {
            }
        });

        toggleBtn.setToggleOn();
        toggleBtn . setToggleOff();
        // no animation toggle
        toggleBtn . setToggleOn( false );
        toggleBtn.setToggleOff(false);

        // Disable the animation
        toggleBtn . setAnimate ( false );*/
    }


    @SuppressLint({"NewApi", "WrongConstant"})
    private void loadLayout() {

        settingsAdapter = new SettingsAdapter(this);
        recyclerViewsettings.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewsettings.setAdapter(settingsAdapter);
        recyclerViewsettings.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }


    private void initMenus() {

        //// Original download call Api //////
        int[] settingsImage = {R.drawable.pwd, R.drawable.payment, R.drawable.print, R.drawable.choose, R.drawable.watermark, R.drawable.watermark, R.drawable.pin, R.drawable.terms, R.drawable.logout2};

        String[] settingsText = {getString(R.string.changepassword), getString(R.string.paymentmethod), getString(R.string.autoprint), getString(R.string.choosewatermark), getString(R.string.enablewatermark), getString(R.string.restorepurchase), getString(R.string.pinsetstate), getString(R.string.terms_lavel), getString(R.string.logout)};


        for (int i = 0; i < settingsImage.length; i++) {
            datasSettings.add(new SettingsModel(settingsImage[i], settingsText[i]/*, settingsState[i]*/));
        }

        settingsAdapter.setDataSettings(datasSettings);

    }



    /////////////// MyLinks Process ////////////
    public void onClickSettingsItem(int pos) {
//        showToast("Settings Item" + pos);
        switch (pos){
            case 0:
                changePassword();
                break;

            case 1:
                Intent intent = new Intent(this, PaymentActivity.class);
                startActivity(intent);
                break;

            case 3:
                Intent intent1 = new Intent(this, WaterMarkActivity.class);
                startActivity(intent1);
                break;

            case 6:
                int myPINCode = EasyPreference.with(SettingsActivity.this)
                        .getInt("easyPINCode", 0);

                if (myPINCode == 0){
                    pinSet(pos, myPINCode);
                }else{
                    pinReset(pos, myPINCode);
                }
                break;

            case 7:
                onTerms();
                break;

            case  8:
                onLogout();
                break;
        }
    }


    private void changePassword(){

        int myID = EasyPreference.with(this)
                .getInt("easyUserId", 0);

        String myPassword = EasyPreference.with(this)
                .getString("easyPassword", "");

        View passDialogView = LayoutInflater.from(this).inflate(R.layout.alert_password_change_confirm, null, false);
        AlertDialog passDialog = new AlertDialog.Builder(this).create();
        passDialog.setView(passDialogView);

        EditText txvPassCurrent = (EditText) passDialogView.findViewById(R.id.edit_current_pass);
        EditText txvPassNew = (EditText) passDialogView.findViewById(R.id.edit_new_pass);
        EditText txvPassConfirm = (EditText) passDialogView.findViewById(R.id.edit_confirm_pass);
        TextView txvPassCancel = (TextView) passDialogView.findViewById(R.id.tv_pass_cancel);
        TextView txvPassOk = (TextView) passDialogView.findViewById(R.id.tv_pass_ok);

        txvPassCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passDialog.hide();
            }
        });


        txvPassOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( txvPassCurrent.getText().toString().isEmpty() || txvPassNew.getText().toString().isEmpty() || txvPassConfirm.getText().toString().isEmpty()){

                    showToast(getText(R.string.wrong_pwd).toString());
                    return;
                }
                if ( myPassword.equals(txvPassCurrent.getText().toString() )   ){

                    if (txvPassNew.getText().toString().equals(txvPassConfirm.getText().toString()) ){

                        passDialog.hide();

                        showProgress();

                        ApiManager.changePassword(myID, txvPassNew.getText().toString(), new ICallback() {

                            @Override
                            public void onCompletion(RESULT result, Object resultParam) {

                                hideProgress();

                                switch (result)
                                {
                                    case FAILURE:

                                        if (resultParam == null){

                                            showToast(getText(R.string.nointernet).toString());
                                        }
                                        else {
                                            showToast(getText(R.string.email_not_exist).toString());
                                        }
                                        break;

                                    case SUCCESS:
                                        showToast(getText(R.string.password_sent).toString());

                                        EasyPreference.with(SettingsActivity.this)
                                                .addString("easyPassword", txvPassNew.getText().toString())
                                                .save();

                                        break;
                                }
                            }
                        });

                    }
                    else
                    {
                        showToast(getText(R.string.confirm_pwd).toString());
                    }
                }
                else {
                    showToast(getText(R.string.incorrect_password).toString());
                }

            }
        });


        passDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        passDialog.show();
    }


    private void pinSet(int pos, int myPINCode){

        String email = EasyPreference.with(this)
                .getString("easyEmail", "");


        View passDialogView = LayoutInflater.from(this).inflate(R.layout.alert_pin_code_confirm, null, false);
        AlertDialog passDialog = new AlertDialog.Builder(this).create();
        passDialog.setView(passDialogView);

        EditText txvPinNew = (EditText) passDialogView.findViewById(R.id.edit_pin);
        EditText txvPinConfirm = (EditText) passDialogView.findViewById(R.id.edit_pin_confirm);
        TextView txvPinCancel = (TextView) passDialogView.findViewById(R.id.tv_pin_cancel);
        TextView txvPinOk = (TextView) passDialogView.findViewById(R.id.tv_pin_set);

        txvPinCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passDialog.hide();
            }
        });

        txvPinOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txvPinNew.getText().toString().isEmpty() || txvPinConfirm.getText().toString().isEmpty()){

                    showToast(getText(R.string.wrong_pin).toString());
                    return;
                }

                if (txvPinNew.getText().toString().equals(txvPinConfirm.getText().toString()) ){

                    EasyPreference.with(SettingsActivity.this)
                            .addInt("easyPINCode", Integer.parseInt(txvPinNew.getText().toString()))
                            .save();

                    passDialog.hide();
                }
                else
                {
                    showToast(getText(R.string.confirmpincode).toString());
                }

            }
        });

        passDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        passDialog.show();
    }

    private void pinReset(int pos, int myPINCode){

        String email = EasyPreference.with(this)
                .getString("easyEmail", "");

        View passDialogView = LayoutInflater.from(this).inflate(R.layout.alert_pin_code_reset, null, false);
        AlertDialog passDialog = new AlertDialog.Builder(this).create();
        passDialog.setView(passDialogView);

        EditText txvPinCurrent = (EditText) passDialogView.findViewById(R.id.edit_cur_pin);
        EditText txvPinNew = (EditText) passDialogView.findViewById(R.id.edit_new_pin);
        EditText txvPinConfirm = (EditText) passDialogView.findViewById(R.id.edit_confirm_pin);
        TextView txvPinCancel = (TextView) passDialogView.findViewById(R.id.tv_pin_cancel);
        TextView txvPinOk = (TextView) passDialogView.findViewById(R.id.tv_pin_set);
        TextView txvPinReset = (TextView) passDialogView.findViewById(R.id.tv_pin_reset);

        txvPinCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*//////////////   Hide SoftKeyboard ///////////////////
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }*/
                passDialog.hide();
            }
        });

        txvPinOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showToast(txvPinCurrent.getText().toString() + txvPinNew.getText().toString() + txvPinConfirm.getText().toString());
                if ( txvPinCurrent.getText().toString().isEmpty() || txvPinNew.getText().toString().isEmpty() || txvPinConfirm.getText().toString().isEmpty()){

                    showToast(getText(R.string.wrong_pin).toString());
                    return;
                }
                if ( myPINCode == Integer.parseInt(txvPinCurrent.getText().toString())   ){

                    if (txvPinNew.getText().toString().equals(txvPinConfirm.getText().toString()) ){

                        EasyPreference.with(SettingsActivity.this)
                                .addInt("easyPINCode", Integer.parseInt(txvPinNew.getText().toString()))
                                .save();

                       /* EasyPrefClass one = EasyPreference.with(SettingsActivity.this).getObject("easySettingStates", EasyPrefClass.class);
                        one.selectedStates[6] = true;

                        EasyPreference.with(SettingsActivity.this).addObject("easySettingStates", one).save();*/

                        passDialog.hide();
                    }
                    else
                    {
                        showToast(getText(R.string.confirmpincode).toString());
                    }
                }
                else {
                    showToast(getText(R.string.currentpincode).toString());
                }
            }
        });

        txvPinReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goApi(email, myPINCode);

                //////////// UI VERSION ////////////
                /*EasyPreference.with(SettingsActivity.this)
                        .addInt("easyPINCode", 0)
                        .save();*/

                passDialog.hide();

            }
        });

        passDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        passDialog.show();
    }


    private void goApi(String email, int pin){

        showProgress();

        ApiManager.resetPINCode(email, pin, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:

                        if (resultParam == null){

                            showToast(getText(R.string.nointernet).toString());
                        }
                        else {
                            showToast(getText(R.string.email_not_exist).toString());
                        }
                        break;

                    case SUCCESS:
                        showToast(getText(R.string.pin_sent).toString());

                        EasyPreference.with(SettingsActivity.this)
                                .remove("easyPINCode")
                                .save();

                        /*EasyPrefClass one = EasyPreference.with(SettingsActivity.this).getObject("easySettingStates", EasyPrefClass.class);
                        one.selectedStates[6] = false;

                        EasyPreference.with(SettingsActivity.this).addObject("easySettingStates", one).save();*/

                        break;
                }
            }
        });
    }

    void onLogout(){

        EasyPreference.with(this).addInt("easyRegistState", 0).save();
        Intent intent = new Intent(this, SigninActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); /// Once remove the Activities between this and HomeActivity opened before already

        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_logout_confirm, null, false);
        androidx.appcompat.app.AlertDialog pinDialog = new AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);

        TextView txvCancel = (TextView) pinDialogView.findViewById(R.id.tv_cancel);
        TextView txvLogout = (TextView) pinDialogView.findViewById(R.id.tv_logout);
        txvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });
        txvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pinDialog.hide();

                startActivity(intent);
                setResult(RESULT_OK); /// HoemActivity off.
                finish();
            }
        });
        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();
    }


    void onTerms(){
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.mysettings_back)
    void onMyLinksBack(){
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}

///////////**************** SettingsAdapter Adapter ***************//////////////////
class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {

    SettingsActivity context;
    ArrayList<SettingsModel> datasourceSettings = new ArrayList<>();
    private LayoutInflater mInflater;

    EasyPrefClass one;

    public SettingsAdapter(SettingsActivity context) {

        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }


    public void setDataSettings(ArrayList<SettingsModel> list) {

        this.datasourceSettings = list;
        notifyDataSetChanged();
    }


    // inflates the cell layout from xml when needed
    @Override
    public SettingsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        /////  EasyPreference Object Getting and NULL process ///////////
        one = EasyPreference.with(context).getObject("easySettingStates", EasyPrefClass.class);

        View view = mInflater.inflate(R.layout.item_settins, parent, false);
        return new SettingsAdapter.ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(SettingsAdapter.ViewHolder holder, int position) {

//        one = EasyPreference.with(context).getObject("easySettingStates",EasyPrefClass.class);

        SettingsModel dataSetting = datasourceSettings.get(position);

        holder.settingsImg.setImageResource(dataSetting.getImgSrc());
        holder.settingsText.setText(String.valueOf(dataSetting.getItemTitle()));


        if (one.settingStates[position] == 1  && one.selectedStates[position]) {
            holder.settingsSwithchButton.setVisibility(View.VISIBLE);
            holder.settingsSwithchButton.setChecked(true);

        }
        if (one.settingStates[position] == 1  && !one.selectedStates[position]) {
            holder.settingsSwithchButton.setVisibility(View.VISIBLE);
            holder.settingsSwithchButton.setChecked(false);
        }



        if (one.settingStates[position] == -1/* && one.selectedStates[position]*/) {
            holder.settingsPinState.setVisibility(View.VISIBLE);
        }
        else
            holder.settingsPinState.setVisibility(View.INVISIBLE);/*
        if (one.settingStates[position] == -1 && !one.selectedStates[position]) {
            holder.settingsPinState.setVisibility(View.INVISIBLE);
        }*/


        //// Item collection Click : Toggle State  /////////
        holder.dataSettingsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.settingsSwithchButton.isChecked() ) {

                    holder.settingsSwithchButton.setChecked(false);
                    one.selectedStates[position] = false;
                    EasyPreference.with(context).addBoolean("easyEnableWaterMark", false).save();
                } else if( position == 2 || position == 4 ){

                    holder.settingsSwithchButton.setChecked(true);
                    one.selectedStates[position] = true;
                    EasyPreference.with(context).addBoolean("easyEnableWaterMark", true).save();
                }


                if (position == 0 || position == 6 )
                {
                    InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                }

                context.onClickSettingsItem(position);

                EasyPreference.with(context)
                        .addObject("easySettingStates", one)
                        .save();
            }
        });


        /////////// SwitchButton Click : Toggle State /////////
        holder.settingsSwithchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                    one.selectedStates[position] = isChecked;

                EasyPreference.with(context).addBoolean("easyEnableWaterMark", isChecked).save();

                EasyPreference.with(context)
                        .addObject("easySettingStates", one)
                        .save();
            }
        });
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceSettings.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.img_settingsimage)ImageView settingsImg;
        @BindView(R.id.tv_settings_text) TextView settingsText;
        @BindView(R.id.tv_pin_state) TextView settingsPinState;
        @BindView(R.id.switch_button) SwitchButton settingsSwithchButton;
        @BindView(R.id.lin_settings_item) LinearLayout dataSettingsItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}