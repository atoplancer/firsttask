package com.pmg.pixinx.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.UserModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends BaseActivity {

    /// Intent related constant
    private final int REQUEST_CODE_SIGNUP = 100;

    /// Network related constant
    private static final int TIMEOUT = 60;

    UserModel me;

    @BindView(R.id.edt_email) EditText email;
    @BindView(R.id.edt_pwd) EditText pass;

    @BindView(R.id.lin_signin) LinearLayout linSignin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ButterKnife.bind(this);

        ///////////////////// click View for hide keyboard //////
        linSignin.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });

    }


    @OnClick(R.id.tv_signin)
    void onSignin() {

        String mail = email.getText().toString();
        String pwd = pass.getText().toString();

        if(!validation(mail, pwd))
            return;

        //If API Communication result is true
        goApi(mail, pwd);

        /////////// UI VERSION ////////////
        /*if(compareEasyPrefData(mail, pwd))
            goHome();
        else {
            showToast("Failed Signin!");
        }*/
    }


    private void goApi(String mail, String pwd){

        showProgress();

        ApiManager.signin(mail, pwd, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:

                        if (resultParam == null){

                            showToast(getText(R.string.nointernet).toString());
                        }
                        else {

                            int resultCode = (int) resultParam;

                            if(resultCode == 104){

                                showToast(getText(R.string.user_not_exist).toString());
                            }
                            else if(resultCode == 105){

                                showToast(getText(R.string.wrong_pwd).toString());
                            }
                        }
                        break;

                    case SUCCESS:

                        JsonObject userInfo = (JsonObject) resultParam;
                        me = new UserModel(userInfo);

                        EasyPreference.with(SigninActivity.this)
                                .addInt("easyUserId", me.getId())
                                .addString("easyUserName", me.getUsername())
                                .addString("easyEmail", me.getEmail())
                                .addString("easyPassword", pwd)
                                .addInt("easyRegistState", 1)
                                .save();

                        goHome();
                        break;
                }
            }
        });

        /////////////////////////  Original Integration Test (Success)  //////////////////////////
        /*showProgress();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(TIMEOUT, TimeUnit.SECONDS);

        OkHttpClient client = builder.build();

        String url = "http://pixinx.com/api/login";

        RequestBody body = new FormBody.Builder()
                .add("email", mail)
                .add("password", pwd)
                .add("device_type", "1")
                .build();

        Request request = new Request.Builder().url(url).post(body).build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                showToast(getText(R.string.nointernet).toString());
                hideProgress();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try{

                    hideProgress();

                    String resp = response.body().string();


                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    switch (resultCode) {
                        case 200:
                            JsonObject userInfo = json.get("user_info").getAsJsonObject();
                            me = new UserModel(userInfo);

                            EasyPreference.with(SigninActivity.this)
                                    .addInt("easyUserId", me.getId())
                                    .addString("easyUserName", me.getUsername())
                                    .addString("easyEmail", me.getEmail())
                                    .addInt("easyRegistState", 1)
                                    .save();

                            goHome();

                            break;
                        case 104:
                            showToast(getText(R.string.user_not_exist).toString());
                            break;
                        case 105:
                            showToast(getText(R.string.wrong_pwd).toString());
                            break;
                    }
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });*/
    }


    ///////////// UI VERSION /////////////////
    /*///// compare Data of EasyPreference
    private boolean compareEasyPrefData(String mail, String pwd){

        String myUserName = EasyPreference.with(this)
                .getString("easyUserName", "");
        String myEmail = EasyPreference.with(this)
                .getString("easyEmail", "");
        String myPassword = EasyPreference.with(this)
                .getString("easyPassword", "");

        if (mail.equals(myUserName) || mail.equals(myEmail) && pwd.equals(myPassword))
            return true;
        else
            return false;
    }*/


    @OnClick(R.id.tv_signup)
    void onSignup() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SIGNUP);
    }


    @OnClick(R.id.tv_forgot)
    void onForgot() {
        Intent intent = new Intent(this, ForgotActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SIGNUP) {

            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    @OnClick(R.id.tv_terms)
    void onTerms() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }


    /////////////////   Go HomeActivity /////////////////////
    private void goHome(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


    /***************  Validtion Function *******************/
    private  boolean validation(String mail, String pwd){

        boolean flag = false, flag1 = false;

        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag =true;
        }
        else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag =false;
        }

        // Password validation
        if (pwd.isEmpty()){
            pass.setBackgroundResource(R.drawable.edit_red_line);
            flag1 = false;
        }
        else {
            pass.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag1 =true;
        }

        return flag & flag1;
    }

}
