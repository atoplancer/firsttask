package com.pmg.pixinx.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.UserModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends BaseActivity {

    @BindView(R.id.edt_user) EditText username;
    @BindView(R.id.edt_email) EditText email;
    @BindView(R.id.edt_pwd) EditText pass;
    @BindView(R.id.edt_confirm) EditText c_pass;
    @BindView(R.id.chk_robot) CheckBox checkBox;
    @BindView(R.id.rl_chk) RelativeLayout rlCheck;

    @BindView(R.id.lin_signup)
    LinearLayout linSignup;

    UserModel me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);

        ///////////////////// click View for hide keyboard //////
        linSignup.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }

        });
    }


    @OnClick(R.id.tv_signin)
    void onSignin() {
        finish();
    }


    @OnClick(R.id.tv_signup)
    void onSignup() {

        String uname = username.getText().toString();
        String mail = email.getText().toString();
        String pwd = pass.getText().toString();
        String cpwd = c_pass.getText().toString();

        if(!validation(uname, mail, pwd, cpwd))
            return;
        if ( !checkBox.isChecked() )
        {
            rlCheck.setBackgroundResource(R.drawable.edit_red_line);
            return;
        }
        else
        {
            rlCheck.setBackgroundResource(R.drawable.sh_edit_text_inactive);
        }

        //If API Communication reuslt is true( Correctly add)
        goApi(uname, mail, pwd);

        /////////////  UI VERSION  ////////////////
        /*saveProfileEasyPref(uname, mail, pwd);    // call save function of my profile data

            /////// SigninActivity unclosed case  //////////
        goHome();
        setResult(RESULT_OK);
        finish();*/
            /////// SigninActivity unclosed case  //////////
    }




    private void goApi(String uname, String mail, String pwd){
        showProgress();

        ApiManager.signup(uname, mail, pwd, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:

                        if (resultParam == null){

                            showToast(getText(R.string.nointernet).toString());
                        }
                        else {

                            int resultCode = (int) resultParam;

                            if(resultCode == 102){

                                showToast(getText(R.string.user_not_exist).toString());
                            }
                            else if(resultCode == 103){

                                showToast(getText(R.string.wrong_pwd).toString());
                            }
                        }
                        break;

                    case SUCCESS:

                        int resultCode = (int) resultParam;

                        EasyPreference.with(SignupActivity.this)
                                .addInt("easyUserId", resultCode)
                                .addString("easyUserName", uname)
                                .addString("easyEmail", mail)
                                .addString("easyPassword", pwd)
                                .addInt("easyRegistState", 1)
                                .save();

                        goHome();
                        break;
                }
            }
        });
    }


    //////////////  UI VERSION  //////////////
    /*//////////////////////  Profile Data Save ////////////////////
    private void saveProfileEasyPref(String uname, String mail, String pwd){

        EasyPreference.with(this)
                .addString("easyUserName", uname)
                .addString("easyEmail", mail)
                .addString("easyPassword", pwd)
                .save();
    }*/



    @OnClick(R.id.tv_terms)
    void onTerms() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }


    private void goHome(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        setResult(RESULT_OK);
        finish();
    }

    /***************  Validtion Function *******************/
    private  boolean validation(String name, String mail, String pwd, String cpwd){

        boolean flag = false, flag1 = false, flag2 = false, flag3 = false, flag4 = false;

        if(name.isEmpty()) {
            username.setBackgroundResource(R.drawable.edit_red_line);
            flag = false;
        }
        else{
            username.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag = true;
        }

        // E-mail validation
        String emailInput = mail.trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (emailInput.matches(emailPattern)) {
            email.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag1 =true;
        }
        else {
            email.setBackgroundResource(R.drawable.edit_red_line);
            flag1 = false;
        }


        // Password validation
        if (pwd.isEmpty()) {
            pass.setBackgroundResource(R.drawable.edit_red_line);
            flag2 = false;
        }
        else {
            pass.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag2 = true;
        }
        if (cpwd.isEmpty()) {
            c_pass.setBackgroundResource(R.drawable.edit_red_line);
            flag3 = false;
        }
        else {
            c_pass.setBackgroundResource(R.drawable.sh_edit_text_inactive);
            flag3 = true;
        }
        if(pwd.equals(cpwd))
            flag4 = true;
        else {
            showToast(getText(R.string.confirm_pwd).toString());
            flag4 = false;
        }


        return flag & flag1 & flag2 & flag3 & flag4;
    }
}
