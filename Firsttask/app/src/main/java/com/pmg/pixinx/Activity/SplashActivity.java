package com.pmg.pixinx.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

public class SplashActivity extends BaseActivity {

    static final int SPLASH_DELAY = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        EasyPreference.with(this).addBoolean("easyEnableWaterMark", false).save();

            new Handler().postDelayed(new Runnable() {

                int registState = EasyPreference.with(SplashActivity.this)
                        .getInt("easyRegistState", 0);

                @Override
                public void run() {

                    if (registState == 1) {
                        home();
                    }
                    else
                    {
                        login();
                    }
                }
            }, SPLASH_DELAY );

    }

    private void login(){
        Intent intent = new Intent(this, SigninActivity.class);
        startActivity(intent);
        finish();
    }

    private void home(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

}
