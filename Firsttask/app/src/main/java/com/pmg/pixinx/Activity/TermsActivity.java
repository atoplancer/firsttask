package com.pmg.pixinx.Activity;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;

import android.widget.TextView;

import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.R;

import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsActivity extends BaseActivity {

    @BindView(R.id.tv_terms) TextView tvterms;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        ButterKnife.bind(this);

        String result;
        try {

            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.terms);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);

        } catch (Exception e) {

            // e.printStackTrace();
            result = "Error: can't show file.";

        }

        tvterms.setText(result);
    }

    @OnClick(R.id.back)
    void onBack(){
        finish();
    }
}
