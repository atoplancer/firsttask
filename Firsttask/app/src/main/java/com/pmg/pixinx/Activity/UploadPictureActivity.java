package com.pmg.pixinx.Activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.GalleryDataModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadPictureActivity extends BaseActivity {

    public static final int REQUEST_CODE_UPLOADPIC = 100;
    public final int REQUEST_REMOTE_PICTURE = 101;
    public static final int COUNT = 10;


    GalleryUploadAdapter galleryAdapter;
    ArrayList<GalleryDataModel> datasGallery = new ArrayList<>();

    @BindView(R.id.recycler_gallery) RecyclerView recyclerViewGallery;

    ///// Intent Data Extra  //////
    Intent intent;
    String bCamera;
    String fCost = "";  /// Data send to UpLOadImages
    String linkUrl = ""; /// Data send to GetLInk
    int countPicture = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploadpicture);

        ButterKnife.bind(this);
        loadLayout();
        initMenus();

        ///////// Camera Type and Cost ///////////////
        intent = getIntent();
        bCamera = intent.getStringExtra("camera");
        fCost = intent.getStringExtra("cost");

        ///////////// ItemTouchHelperCallback /////////////
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(galleryAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerViewGallery);
    }


    @SuppressLint("NewApi")
    private void loadLayout() {

        galleryAdapter = new GalleryUploadAdapter(this);
        recyclerViewGallery.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewGallery.setAdapter(galleryAdapter);
    }


    private void initMenus() {
        //// Original download call Api //////
        /*int[] galleryImage = {R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo, R.drawable.photo};
        for (int i = 0; i < galleryImage.length; i++) {
            datasGallery.add(new GalleryDataModel(String.valueOf(galleryImage[i])));
        }*/
        galleryAdapter.setDataGallery(datasGallery);
    }


    /*/////////////// Gallery Process ////////////
    public void onClickGallery(int pos) {
        showToast("Gallery Photo" + pos);
        // process.
    }*/


    public void onClickClose(int pos) {
        datasGallery.remove(pos);
        galleryAdapter.notifyDataSetChanged();
        // process.
    }


    @OnClick(R.id.img_uploadpicture)
    void onUploadPicture(){

        if ( COUNT - countPicture ==0)
        {
            showToast(getText(R.string.max_reached).toString());
            return;
        }

        if (bCamera.equals("local")){
            ImagePicker.cameraOnly().start(this); // Could be Activity, Fragment, Support Fragment
        }

        else {

            /////////// Send REQUEST_REMOTE_PICTURE to RemotePicturesActivity ////////////
            Intent intent = new Intent(this, RemotePicturesActivity.class);
            intent.putExtra("COUNT", ( COUNT - countPicture ) );
            startActivityForResult(intent, REQUEST_REMOTE_PICTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_UPLOADPIC) {

            if (resultCode == RESULT_OK) {
                finish();
            }
            return;
        }


        /////////////  Get data from RemotePicturesActivity /////////////////
        if (requestCode == REQUEST_REMOTE_PICTURE) {

            if (resultCode == RESULT_OK) {

                ArrayList<String> temps = data.getStringArrayListExtra(RemotePicturesActivity.INTENT_DATA_PICTURES);

                for (String one : temps) {
                    GalleryDataModel temp = new GalleryDataModel(one);
                    datasGallery.add(temp);
                    countPicture = countPicture + 1;
                }

                galleryAdapter.notifyDataSetChanged();
            }

            return;
        }

        // Image Gallery
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            for (Image one : images )
            {
                datasGallery.add(new GalleryDataModel(one.getPath()));
                galleryAdapter.notifyDataSetChanged();
                countPicture++;
            }
            // or get a single image only
          /* Image image = ImagePicker.getFirstImageOrNull(data);
           pictures.add(new PictureModel(image.getPath()));
           adapter.notifyDataSetChanged();*/
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    @OnClick(R.id.booth_tv_submit)
    void onSunbmit(){

        if ( datasGallery.size() == 0){

            View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_select_pic, null, false);
            AlertDialog pinDialog = new AlertDialog.Builder(this).create();
            pinDialog.setView(pinDialogView);

            TextView txvOk = (TextView) pinDialogView.findViewById(R.id.tv_ok);

            txvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pinDialog.hide();
                }
            });

            pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pinDialog.show();
            return;
        }

        List<String> images = new ArrayList<>();

        for (GalleryDataModel one : datasGallery )
        {
            try {
                images.add( one.getImageSrc() );
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        upLoadImages(images);

    }


    ////////////// Upload Images ///////////////////

    private void upLoadImages(List<String> images) {

        showProgress();

        Intent intent = new Intent(this, GetLinkBoothActivity.class);

        int userId = EasyPreference.with(this).getInt("easyUserId", 0);
        int waterMark = EasyPreference.with(this).getInt("easyWaterMark", 0);
        boolean enableWaterMark = EasyPreference.with(this).getBoolean("easyEnableWaterMark", false);

        ApiManager.uploadImages(enableWaterMark, userId, fCost, waterMark, images, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonObject link_info = (JsonObject) resultParam;

                        linkUrl = link_info.get("url").getAsString();

                        intent.putExtra("link_url", linkUrl);

                        startActivityForResult(intent, REQUEST_CODE_UPLOADPIC);

                }
            }
        });
    }

    @OnClick(R.id.booth_tv_quit)
    void onQuit(){

        int myPINCode = EasyPreference.with(this)
                .getInt("easyPINCode", 0);

        View pinDialogView = LayoutInflater.from(this).inflate(R.layout.alert_pin_code, null, false);
        AlertDialog pinDialog = new AlertDialog.Builder(this).create();
        pinDialog.setView(pinDialogView);

        TextView txvPinCancel = (TextView) pinDialogView.findViewById(R.id.tv_pin_cancel);
        TextView txvPinOk = (TextView) pinDialogView.findViewById(R.id.tv_pin_ok);

        TextView txvPin = (TextView) pinDialogView.findViewById(R.id.edit_pin);

        txvPinCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinDialog.hide();
            }
        });

        txvPinOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txvPin.getText().toString().isEmpty())
                    return;

                if (myPINCode == Integer.parseInt(txvPin.getText().toString()))
                {
                    pinDialog.hide();
                    setResult(RESULT_OK);
                    finish();
                }
                else
                {
                    showToast("Confirm PINCode!");
                }
            }
        });

        pinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pinDialog.show();
    }

}

///////////////GALLERY Adapter //////////////////
class GalleryUploadAdapter extends RecyclerView.Adapter<GalleryUploadAdapter.ViewHolder> implements SimpleItemTouchHelperCallback.ItemTouchHelperAdapter {

    UploadPictureActivity context;
    private LayoutInflater mInflater;
    ArrayList<GalleryDataModel> datasourceGallery = new ArrayList<>();

    public GalleryUploadAdapter(UploadPictureActivity context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDataGallery(ArrayList<GalleryDataModel> list) {
        this.datasourceGallery = list;
        notifyDataSetChanged();
    }

    // inflates the cell layout from xml when needed
    @Override
    public GalleryUploadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_gallery, parent, false);
        return new GalleryUploadAdapter.ViewHolder(view);
    }
    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(GalleryUploadAdapter.ViewHolder holder, int position) {

        GalleryDataModel dataGallery = datasourceGallery.get(position);
//        holder.galleryImage.setImageResource(Integer.parseInt(dataGallery.getImageSrc()));

        /*holder.galleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickGallery(position);
            }
        });*/
        holder.galleryClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickClose(position);
            }
        });
        /// Display Images selected in Gallery
        Glide.with(context).load(dataGallery.getImageSrc()).into(holder.galleryImage);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceGallery.size();
    }

    ///////////////  Drag and Drop ///////////
    @Override
    public void onItemDismiss(int position) {
        datasourceGallery.remove(position);
//        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public void  onItemMove(int fromPosition, int toPosition) {

        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(datasourceGallery, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(datasourceGallery, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        //notifyDataSetChanged();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.galleryimage)
        ImageView galleryImage;
        @BindView(R.id.galleryclose)
        ImageView galleryClose;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}


////////////////////////////////////////// SimpleItemtouchCalss for Movenent Item /////////////////////////////////////
class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final GalleryUploadAdapter mAdapter;

    public SimpleItemTouchHelperCallback(
            GalleryUploadAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        int swipeFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(),
                target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

    public interface ItemTouchHelperAdapter {

        void onItemMove(int fromPosition, int toPosition);

        void onItemDismiss(int position);
    }
}