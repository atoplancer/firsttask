package com.pmg.pixinx.Activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.WaterMarkModel;
import com.pmg.pixinx.R;
import com.pmg.pixinx.network.ApiManager;
import com.pmg.pixinx.network.ICallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WaterMarkActivity extends BaseActivity {

    WaterMarkAdapter watermarkAdapter;
    ArrayList<WaterMarkModel> datasWaterMark = new ArrayList<>();

    WaterMarkModel waterMarkModel;

    int waterMarkPosition = -1;

    @BindView(R.id.recycler_watermark) RecyclerView recyclerViewWaterMark;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watermark);

        ButterKnife.bind(this);

        loadLayout();

        loadWaterMark();
    }


    @SuppressLint("NewApi")
    private void loadLayout() {

        watermarkAdapter = new WaterMarkAdapter(this);
        recyclerViewWaterMark.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewWaterMark.setAdapter(watermarkAdapter);
    }


    private void loadWaterMark() {

        showProgress();

        int userId = EasyPreference.with(this).getInt("easyUserId", 0);
        int waterMarkId = EasyPreference.with(this).getInt("easyWaterMark", 0);

        ApiManager.receiveWaterMark(userId, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonArray imagesJsonArray = (JsonArray) resultParam;

                        if (imagesJsonArray.size() != 0) {

                            for (int i=0 ; i < imagesJsonArray.size(); i++){

                                JsonObject json = imagesJsonArray.get(i).getAsJsonObject();

                                if (json.get("id").getAsInt() == waterMarkId)
                                {
                                    datasWaterMark.add(new WaterMarkModel(json.get("id").getAsInt(), json.get("url").getAsString(), true));

                                }
                                else {
                                    datasWaterMark.add(new WaterMarkModel(json.get("id").getAsInt(), json.get("url").getAsString()));

                                }

                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    watermarkAdapter.setDataWaterMark(datasWaterMark);
                                }
                            });

                        }
                        else
                        {
                            showToast(getText(R.string.no_water_images).toString());
                        }
                        break;
                }
            }
        });
    }

    @OnClick(R.id.tv_watermarkadd)
    void onWaterMarkAdd(){
        ImagePicker.create(this)
                .returnMode(ReturnMode.NONE) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle(getString(R.string.imagegalleryfolders)) // folder selection title
                .toolbarImageTitle(getString(R.string.selectemptypic)) // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .includeVideo(false) // Show video on image picker
                .single() // single mode
                //.multi() // multi mode (default mode)
                //.limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                //.origin(images) // original selected images, used in multi mode
                //.exclude(images) // exclude anything that in image.getPath()
                //.excludeFiles(files) // same as exclude but using ArrayList<File>
                //.theme(R.style.CustomImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log
                //.imageLoader(new GrayscaleImageLoder()) // custom image loader, must be serializeable
                .start(); // start image picker activity with request code
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);

            //////// At the time new add, reset selected state of previous.
            for (WaterMarkModel oneWater : datasWaterMark)
            {
                oneWater.setCheckedState(false);
            }

            for (Image one : images )
            {
                datasWaterMark.add(new WaterMarkModel(one.getPath(), true)); ////////// display to selected state.
                watermarkAdapter.notifyDataSetChanged();
            }

            Image image = ImagePicker.getFirstImageOrNull(data);

            upLoadWaterMark(image.getPath()); ///////  Selected Gallery Upload to Server.
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    ////////////// Upload WaterMark ///////////////////

    private void upLoadWaterMark(String imageSrc) {

        showProgress();

        File file = new File(imageSrc);

        int userId = EasyPreference.with(this).getInt("easyUserId", 0);

        ApiManager.uploadWatermark(userId, file, new ICallback() {

            @Override
            public void onCompletion(RESULT result, Object resultParam) {

                hideProgress();

                switch (result)
                {
                    case FAILURE:
                        showToast(getText(R.string.nointernet).toString());
                        break;

                    case SUCCESS:

                        JsonObject json = (JsonObject) resultParam;

                        EasyPreference.with(WaterMarkActivity.this).addInt("easyWaterMark", json.get("watermark_id").getAsInt()).save();

                }
            }
        });
    }


    /////////////// WaterMark Process ////////////

    public void onWaterMark(int position) {

        waterMarkPosition = position; /////// selected position
        EasyPreference.with(WaterMarkActivity.this).addInt("easyWaterMark", datasWaterMark.get(waterMarkPosition).getId()).save();

    }

    @OnClick(R.id.back)
    void onBack(){
        finish();  //////// if selected goes to onPause()
    }

    /*@Override
    protected void onPause() {
        super.onPause();

        if (waterMarkPosition == -1)  /////  if unselected do finish(),  save a Easy Value.
            return;

        waterMarkModel = datasWaterMark.get(waterMarkPosition);
        EasyPreference.with(WaterMarkActivity.this).addInt("easyWaterMark", waterMarkModel.getId()).save();
    }*/
    /////////////// WaterMark Process End ////////////


}

////////////************ WaterMark Adapter ****************//////////////////
class WaterMarkAdapter extends RecyclerView.Adapter<WaterMarkAdapter.ViewHolder> {

    WaterMarkActivity context;
    ArrayList<WaterMarkModel> datasourceWaterMark = new ArrayList<>();

    int waterMarkId;

    private LayoutInflater mInflater;

    public WaterMarkAdapter(WaterMarkActivity context) {

        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        waterMarkId = EasyPreference.with(context).getInt("easyWaterMark", 0);
    }


    public void setDataWaterMark(ArrayList<WaterMarkModel> list) {

        this.datasourceWaterMark = list;
        notifyDataSetChanged();
    }


    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_watermark, parent, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        WaterMarkModel dataWaterMark = datasourceWaterMark.get(position);
        //holder.watermarkImage.setImageResource(Integer.parseInt(dataWaterMark.getImageSrc()));
        //holder.imgCheck.setSelected(dataWaterMark.isCheckedState());

        if (dataWaterMark.isCheckedState())
            holder.imgCheck.setVisibility(View.VISIBLE);
        else
            holder.imgCheck.setVisibility(View.GONE);

        holder.linWaterMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < datasourceWaterMark.size(); i++) {

                    if (i == position) {
                        datasourceWaterMark.get(i).setCheckedState(true);
                    } else {
                        datasourceWaterMark.get(i).setCheckedState(false);
                    }
                }

                context.onWaterMark(position);

                notifyDataSetChanged();
            }
        });

        Glide.with(context).load(dataWaterMark.getImageSrc()).into(holder.watermarkImage);
    }



    // total number of cells
    @Override
    public int getItemCount() {
        return datasourceWaterMark.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.lin_watermark) FrameLayout linWaterMark;
        @BindView(R.id.img_watermark) ImageView watermarkImage;
        @BindView(R.id.img_check) ImageView imgCheck;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}