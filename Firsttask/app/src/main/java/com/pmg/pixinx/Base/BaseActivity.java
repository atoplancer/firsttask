package com.pmg.pixinx.Base;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kaopiz.kprogresshud.KProgressHUD;

public class BaseActivity  extends AppCompatActivity {

    KProgressHUD kProgress;

    public static boolean enableWaterMark = false;

    public void showToast(final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void showToast(int id) {
        showToast(String.valueOf(id));
    }

    public KProgressHUD showProgress(){

        kProgress = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                /*.setLabel("Please wait")*/
                .setCancellable(true)
                .show();
        return kProgress;
    }

    public void hideProgress(){

        if (kProgress != null)
            kProgress.dismiss();
    }


    public static String removeWord(String string, String word)
    {
        if (string.contains(word)) {

            string = string.replaceAll(word, "");

        }

        return string;
    }
}
