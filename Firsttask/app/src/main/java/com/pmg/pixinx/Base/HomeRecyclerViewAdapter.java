package com.pmg.pixinx.Base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pmg.pixinx.Activity.HomeActivity;
import com.pmg.pixinx.Model.ButtonItemDataModel;
import com.pmg.pixinx.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder> {

    HomeActivity context;
    ArrayList<ButtonItemDataModel> datasource = new ArrayList<>();

    private LayoutInflater mInflater;


    public HomeRecyclerViewAdapter(HomeActivity context) {

        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }



    public void setData(ArrayList<ButtonItemDataModel> list) {

        this.datasource = list;
        notifyDataSetChanged();
    }



    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.item_home, parent, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ButtonItemDataModel data = datasource.get(position);

        holder.homeImage.setImageResource(data.getImageSrc());
        holder.homeText.setText(data.getScriptText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.onClickMenu(position);
            }
        });

       /* Click a Image part only
       holder.buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
       /* Click a Text part only
       holder.buttonText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
       /* LongClick
       holder.itemView.setOnLongClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }



    // total number of cells
    @Override
    public int getItemCount() {
        return datasource.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.homeimage) ImageView homeImage;
        @BindView(R.id.hometext) TextView homeText;
        @BindView(R.id.homedata) LinearLayout homeData;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}