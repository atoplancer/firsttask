package com.pmg.pixinx.Model;

public class ButtonItemDataModel {

    private int imageSrc = 0;
    private String scriptText = "";
    private boolean isSelected = false;


    public ButtonItemDataModel(int imageSrc, String scriptText){

        this.imageSrc = imageSrc;
        this.scriptText = scriptText;
    }


    public ButtonItemDataModel(int imageSrc, String scriptText, boolean isSelected){

        this.imageSrc = imageSrc;
        this.scriptText = scriptText;
        this.isSelected = isSelected;
    }


    public int getImageSrc() {
        return imageSrc;
    }


    public void setImageSrc(int imageSrc) {
        this.imageSrc = imageSrc;
    }


    public String getScriptText() {
        return scriptText;
    }


    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }


    public boolean isSelected() {
        return isSelected;
    }


    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
