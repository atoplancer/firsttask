package com.pmg.pixinx.Model;

public class GalleryDataModel {

    private String imageSrc = "";


    public GalleryDataModel(String imageSrc){
        this.imageSrc = imageSrc;
    }


    public String getImageSrc() {
        return imageSrc;
    }


    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

}
