package com.pmg.pixinx.Model;

import android.icu.text.SimpleDateFormat;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

public class SalesModel implements Comparable<SalesModel> {

    private int id = 0;
    private String imgSrc = "";
    private String linkAdress = "";
    private String salesDate = "";
    private float cost = 0.0f;

    private String buyer = "";
    private float commission = 0.0f;



    public SalesModel(int id, String linkAdress, float cost, String imgsrc, String salesDate) {

        this.id = id;
        this.linkAdress = linkAdress;
        this.cost = cost;
        this.imgSrc = imgsrc;
        this.salesDate = salesDate;
    }

    public SalesModel(int id, String buyer, float cost, float commission, String linkAdress, String imgsrc, String salesDate) {

        this.id = id;
        this.buyer = buyer;
        this.cost = cost;
        this.commission = commission;
        this.linkAdress = linkAdress;
        this.imgSrc = imgsrc;
        this.salesDate = salesDate;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public float getCommission() {
        return commission;
    }

    public void setCommission(float commission) {
        this.commission = commission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getLinkAdress() {
        return linkAdress;
    }

    public void setLinkAdress(String linkAdress) {
        this.linkAdress = linkAdress;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }


    //////////////////   Compare Function ( sort of this object ) /////////.///////////////

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int compareTo(SalesModel salesModel) {

        String compareDateString = ((SalesModel) salesModel).getSalesDate();

        /*Calendar compareCal = Calendar.getInstance();
        Calendar thisCal = Calendar.getInstance();*/

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date compareDate = null;
        Date thisDate = null;

        try {
            thisDate = sdf.parse(this.salesDate);
            compareDate = sdf.parse(compareDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /*compareCal.setTime(compareDate);
        thisCal.setTime(thisDate);*/

        return compareDate.compareTo(thisDate);
    }
    ///////////////////  To sort with multiple properties ///////////////
    /*public static Comparator<SalesModel> SalesModeComparator = new Comparator<SalesModel>() {
        @Override
        public int compare(SalesModel salesModel_A, SalesModel salesModel_B) {
            return 1;
            //String fruitName1 = fruit1.getFruitName().toUpperCase();
            //String fruitName2 = fruit2.getFruitName().toUpperCase();

            //ascending order
            //return fruitName1.compareTo(fruitName2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }
    };*/

}
