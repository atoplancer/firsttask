package com.pmg.pixinx.Model;

public class SettingsModel {


    private int imgSrc = 0;
    private String itemTitle = "";
    private int stateFlage = 0;


//    public SettingsModel(int imgsrc, String itemTitle, int stateFlage) {
    public SettingsModel(int imgsrc, String itemTitle) {

        this.imgSrc = imgsrc;
        this.itemTitle = itemTitle;
    }

    public int getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(int imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

}
