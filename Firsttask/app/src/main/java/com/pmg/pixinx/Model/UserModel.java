package com.pmg.pixinx.Model;

import com.google.gson.JsonObject;

import java.util.Date;

public class UserModel {

    private int id = 0;
    private String username = "";
    private String email = "";
    private String profile = "";
    private Date createdAt = new Date();

    public UserModel(JsonObject dict){

        this.id = dict.get("user_id").getAsInt();
        this.username = dict.get("username").getAsString();
        this.email = dict.get("email").getAsString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
