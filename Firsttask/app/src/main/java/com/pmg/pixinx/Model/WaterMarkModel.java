package com.pmg.pixinx.Model;

public class WaterMarkModel {

    private int id = 0;
    private String imageSrc = "";
    private boolean checkedState = false;


    public WaterMarkModel(String imageSrc){

        this.imageSrc = imageSrc;
    }

    public WaterMarkModel(int id, String imageSrc){

        this.id = id;
        this.imageSrc = imageSrc;
    }

    public WaterMarkModel(String imageSrc, boolean checkedState){

        this.id = id;
        this.checkedState = checkedState;
    }

    public WaterMarkModel(int id, String imageSrc, boolean state){

        this.id = id;
        this.imageSrc = imageSrc;
        this.checkedState = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public boolean isCheckedState() {
        return checkedState;
    }

    public void setCheckedState(boolean checkedState) {
        this.checkedState = checkedState;
    }

}
