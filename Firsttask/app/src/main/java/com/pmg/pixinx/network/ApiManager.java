package com.pmg.pixinx.network;

import com.esafirm.imagepicker.model.Image;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.iamhabib.easy_preference.EasyPreference;
import com.pmg.pixinx.Activity.CaptureUploadActivity;
import com.pmg.pixinx.Activity.SettingsActivity;
import com.pmg.pixinx.Activity.SigninActivity;
import com.pmg.pixinx.Base.BaseActivity;
import com.pmg.pixinx.Model.UserModel;
import com.pmg.pixinx.R;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiManager extends BaseActivity {

    ///////// Host Related Constand /////////////
    public static final String HOST = "http://pixinx.com/";
    public static final String API = HOST + "api/";
    public static final String SIGNIN = "login";
    public static final String SIGNUP = "register";
    public static final String FORGOT = "resetPassword";
    public static final String RESETPIN = "resetPin";
    public static final String SETPASSWORD = "setPassword";
    public static final String SAVEPAYPAL = "savePaypal";
    public static final String REMOTE = "getRemoteFiles";
    public static final String MYYLINK = "getMyLink";
    public static final String HISTORY = "getSales";
    public static final String WATERMARK = "getWatermarks";
    public static final String UPLOAD_WATERMARK = "uploadWatermark";
    public static final String UPLOAD_IMAGES = "uploadFiles";
    public static final String GETSETTINGS = "getSettings";


    public static final int RESULT_OK = 200;


    ///************************** Signin page ***************************//

    public static void signin(String email, String pwd, final ICallback callback){

        String url = API + SIGNIN;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("email", email)
                .add("password", pwd)
                .add("device_type", "1")
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        JsonObject userInfo = json.get("user_info").getAsJsonObject();
                        callback.onCompletion(ICallback.RESULT.SUCCESS, userInfo);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }

    ///************************** Signup page ***************************//

    public static void signup(String username, String email, String pwd, final ICallback callback){

        String url = API + SIGNUP;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("email", email)
                .add("username", username)
                .add("password", pwd)
                .add("device_type", "1")
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();
                    int userId = json.get("user_id").getAsInt();

                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, userId);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }

    ///************************** Forgot password page ***************************//

    public static void forgot(String email, final ICallback callback){

        String url = API + FORGOT;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("email", email)
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }

    ///************************** Reset PINCode Set (Can only user, Can't non user, so required Email.) ***************************//

    public static void resetPINCode(String pEmail, int pin, final ICallback callback) {

        String url = API + RESETPIN;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("email", pEmail)
                .add("pin", String.valueOf(pin))
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();


                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });
    }


    ///************************** change password ***************************//

    public static void changePassword(int id, String password, final ICallback callback){

        String url = API + SETPASSWORD;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("user_id", String.valueOf(id))
                .add("password", password)
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });
    }


    ///************************** Paypal Address Getting ***************************//

    public static void payment(int id, String paypalemail, final ICallback callback){

        String url = API + SAVEPAYPAL;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("user_id", String.valueOf(id))
                .add("email", paypalemail)
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, resultCode);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    ///************************** Remote Pictures ***************************//

    public static void remotePictures(String uname, final ICallback callback){

        String url = API + REMOTE;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("username", uname)
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        JsonArray images = json.get("images").getAsJsonArray();
                        callback.onCompletion(ICallback.RESULT.SUCCESS, images);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }

    ///************************** Mylinks page ***************************//

    public static void myLinks(int uid, final ICallback callback){

        String url = API + MYYLINK;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("user_id", String.valueOf(uid))
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        JsonArray links = json.get("links").getAsJsonArray();
                        callback.onCompletion(ICallback.RESULT.SUCCESS, links);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    ///************************** MylHistory page ***************************//

    public static void myHis(int uid, final ICallback callback){

        String url = API + HISTORY;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("user_id", String.valueOf(uid))
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {
                        callback.onCompletion(ICallback.RESULT.SUCCESS, json);
                    } else {
                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    ///************************** Receive WaterMark  ***************************//

    public static void receiveWaterMark(int uid, final ICallback callback){

        String url = API + WATERMARK;   /////// URL Getting.

        RequestBody body = new FormBody.Builder()  //////////////  Request Content.
                .add("user_id", String.valueOf(uid))
                .build();

        WebManager.call(url, WebManager.METHOD.POST, body, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {

                        JsonArray watermarks = json.get("watermarks").getAsJsonArray();
                        callback.onCompletion(ICallback.RESULT.SUCCESS, watermarks);

                    } else {

                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    ///************************** Upload WaterMark  ***************************//

    public static void uploadWatermark(int uid, File file, final ICallback callback){

        String url = API + UPLOAD_WATERMARK;   /////// URL Getting.

        /////////////   an File upload /////////
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("user_id", String.valueOf(uid))
                .addFormDataPart("file", file.getName(), RequestBody.create(file, MediaType.parse("image/*")))
                .build();


        WebManager.call(url, WebManager.METHOD.POST, requestBody, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {

                        callback.onCompletion(ICallback.RESULT.SUCCESS, json);

                    } else {

                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    ///************************** Upload Images  ***************************//

    public static void uploadImages(boolean enableWaterMark, int userId, String fCost, int waterMark, List<String> images, ICallback callback) {

        String url = API + UPLOAD_IMAGES;   /////// URL Getting.

        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);

        builder.addFormDataPart("user_id", String.valueOf(userId));
        builder.addFormDataPart("price", fCost);

        if ( enableWaterMark ){
            builder.addFormDataPart("watermark", String.valueOf(waterMark));
        }else{
            builder.addFormDataPart("watermark", "0");
        }

        List<String> remotes = new ArrayList<>();
        /*String remote = "";*/

        for (String one : images )
        {
            if (one.contains("http"))
            {
                remotes.add(one);
            }
            else
            {
                File file = new File(one);
                final MediaType MEDIA_TYPE = MediaType.parse("image/*");
                builder.addFormDataPart("filename[]",file.getName(),RequestBody.create(file, MEDIA_TYPE));
            }
        }
        //////////// String Making ////////////////////
        /*if (!remotes.isEmpty()){
            for (int i = 0; i < remotes.size(); i++)
            {
                remote = remote + remotes.get(i);
                if (i != remotes.size() - 1)
                {
                    remote = remote + ",";
                }
            }
            builder.addFormDataPart("remote", remote);
        }*/
        ////////////// Making String of ["http://sdfsfdsd/dd","http://dsfsdfs/sfsdf"] ///////////
        JsonArray remotesArr = new JsonArray();

        if (!remotes.isEmpty()){
            for (String one : remotes)
            {
                remotesArr.add(one);
            }
            builder.addFormDataPart("remote", remotesArr.toString());
        }
        //////////// String Making  End ////////////////////

        RequestBody requestBody = builder.build();

        WebManager.call(url, WebManager.METHOD.POST, requestBody, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {

                        JsonObject linkInfo = json.get("link_info").getAsJsonObject();
                        callback.onCompletion(ICallback.RESULT.SUCCESS, linkInfo);

                    } else {

                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });
    }


    ///************************** Get Settings ( printer, helper, minprice URL )  ***************************//

    public static void getSettings( final ICallback callback){

        String url = API + GETSETTINGS;   /////// URL Getting.

        /////////////   an File upload /////////
        RequestBody requestBody = new FormBody.Builder()
                .build();


        WebManager.call(url, WebManager.METHOD.GET, requestBody, new Callback() {    ////// Request call with a url ,method and content.
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                callback.onCompletion(ICallback.RESULT.FAILURE, null);

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try {

                    String resp = response.body().string();

                    JsonObject json = new JsonParser().parse(resp).getAsJsonObject();

                    int resultCode = json.get("result_code").getAsInt();

                    if (resultCode == 200) {

                        callback.onCompletion(ICallback.RESULT.SUCCESS, json);

                    } else {

                        callback.onCompletion(ICallback.RESULT.FAILURE, resultCode);
                    }

                }
                catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

        });

    }


    public static class PARAMS {

        public static final String PASSWORD = "password";
        public static final String EMAIL = "email";
        public static final String DEVICETYPE = "device_type";
    }

}
