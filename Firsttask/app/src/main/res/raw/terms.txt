Collection of Personal Information

Pixinx collectes your basic login information to ensure you can login to the application. This information is not shared by us with any third parties.

Pixinc collects your paypal username to provide payment this information is not shared with any third party other than Paypal for payment processing. 

Pixinc stores your pictures (uploaded or taken) for the sole purpose of helping you distribute this content to your customers, we do not claim any ownership of content and do not share this information with any third parties. 

Pixinx stores your data as long as your account is active, you may request your information and account deleted by sending an email to info@pixinx.com.

Pixinx strives to protect the Personal Information submitted to us, both during transmission and once we receive it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we take into account the sensitivity of the Personal Information we collect, process and store, and the current state of technology to use these measures to protect your Personal Information, we cannot guarantee its absolute security.

Pixinx does not automatically collect information, all the information we store is directly submitted by you. 

Pixinx uses Paypal for credit card processing to bill you for our goods and services. To the best of our knowledge, Paypal does not retain, share, store or use personally identifiable information for any other purpose. Please visit Paypal’s site and review their privacy policy.

Pixinx uses the Apple App store and Alphabits Play Store to host and transmit our applications, please read their privacy policies for more information.

Pixinx reserves the right to amend the Privacy Policy from time to time at its sole discretion and will provide notice by email or on the homepage of the Site when we make material changes to this Privacy Policy prior to the change becoming effective.

